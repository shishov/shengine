﻿#region

using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShEngine.Utitlities;

#endregion

namespace ShEngineTests
{
    /// <summary>
    ///     Это класс теста для AspectTest, в котором должны
    ///     находиться все модульные тесты AspectTest
    /// </summary>
    [TestClass]
    public class AspectTest
    {
        #region Дополнительные атрибуты теста

        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        [TestMethod]
        public void AspectOverAllTest()
        {
            var aspect = new Aspect<int>().AllOf(1, 2, 3).OneOf(4, 5, 6).ExceptOf(8, 9);

            var mylist1 = new int[5] {1, 2, 3, 4, 8};
            var mylist2 = new int[5] {1, 1, 3, 4, 8};
            var mylist3 = new int[4] {1, 2, 3, 4};

            Assert.AreEqual(aspect.Query(mylist1.Contains), false);
            Assert.AreEqual(aspect.Query(mylist2.Contains), false);
            Assert.AreEqual(aspect.Query(mylist3.Contains), true);
        }

        [TestMethod]
        public void AspectOneOfTest()
        {
            var aspect = new Aspect<int>().OneOf(1, 2, 3);

            var mylist1 = new int[1] {1};
            var mylist2 = new int[3] {1, 4, 5};
            var mylist3 = new int[3] {1, 2, 5};
            var mylist4 = new int[2] {4, 5};

            Assert.AreEqual(true, aspect.Query(mylist1));
            Assert.AreEqual(true, aspect.Query(mylist2));
            Assert.AreEqual(false, aspect.Query(mylist3));
            Assert.AreEqual(false, aspect.Query(mylist4));
        }
    }
}