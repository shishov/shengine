﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace ShEngineTests
{
    [TestClass]
    public class TransformTests
    {
        [TestMethod]
        public void ViewToWorld()
        {
            var position = new Vector3(0, 0, 10);
            var world = Matrix.CreateTranslation(position);
            var view = Matrix.Invert(world);

            var lookAtView = Matrix.CreateLookAt(position, Vector3.Zero, Vector3.Up);

            Assert.AreEqual(view, lookAtView);
        }
    }
}
