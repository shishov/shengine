﻿#region

using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShEngine.EntitySystem;

#endregion

namespace ShEngineTests
{
    [TestClass]
    public class EntityTest
    {
        [TestMethod]
        public void HasComponentTest()
        {
            var world = new EntityWorld();
            var e = new Entity(world);
            e.AddComponent(new TestComponent());
            Assert.AreEqual(true, e.HasComponent(typeof (TestComponent)));
            Assert.AreEqual(true, e.HasComponent<TestComponent>());
            Assert.AreEqual(false, e.HasComponent(typeof (TestComponent2)));
            Assert.AreEqual(false, e.HasComponent<TestComponent2>());
        }

        #region Nested type: TestComponent

        private class TestComponent : IComponent
        {
        }

        #endregion

        #region Nested type: TestComponent2

        private class TestComponent2 : IComponent
        {
        }

        #endregion
    }
}