﻿#region

using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShEngine.Utitlities;

#endregion

namespace ShEngineTests
{
    /// <summary>
    ///     Это класс теста для TypeGroupTest, в котором должны
    ///     находиться все модульные тесты TypeGroupTest
    /// </summary>
    [TestClass]
    public class TypeGroupTest
    {
        #region Дополнительные атрибуты теста

        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///     Тест для Add
        /// </summary>
        public void AddTestHelper<T, T1>()
            where T1 : T
        {
            var target = new TypeGroup<T>();
            target.Add<T1>();
            Assert.AreEqual(true, target.Contains<T1>());

            // TODO : ADD DUPLICATE
        }

        [TestMethod]
        public void AddTest()
        {
            AddTestHelper<GenericParameterHelper, GenericParameterHelper>();
        }

        /// <summary>
        ///     Тест для Remove
        /// </summary>
        public void RemoveTestHelper<T, T1>()
            where T1 : T
        {
            var target = new TypeGroup<T>();

            target.Add<T1>();
            Assert.AreEqual(true, target.Contains<T1>());

            target.Remove<T1>();
            Assert.AreEqual(false, target.Contains<T1>());

            // TODO: REMOVE NON-EXISTENT
        }

        [TestMethod]
        public void RemoveTest()
        {
            RemoveTestHelper<GenericParameterHelper, GenericParameterHelper>();
        }

        /// <summary>
        ///     Тест для Contains
        /// </summary>
        public void ContainsTestHelper<T, T1>()
            where T1 : T
        {
            var target = new TypeGroup<T>();
            Assert.AreEqual(target.Contains<T1>(), target.Contains(typeof (T1)));

            target.Add<T1>();
            Assert.AreEqual(target.Contains<T1>(), target.Contains(typeof (T1)));

            target.Remove<T1>();
            Assert.AreEqual(target.Contains<T1>(), target.Contains(typeof (T1)));
        }

        [TestMethod]
        public void ContainsTest()
        {
            ContainsTestHelper<GenericParameterHelper, GenericParameterHelper>();
        }
    }
}