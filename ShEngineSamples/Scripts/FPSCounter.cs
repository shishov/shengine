﻿#region

using System;
using System.Diagnostics;
using NetComponentEntitySystem;

#endregion

namespace ShEngineSamples.Scripts
{
    internal class FpsCounter : Script
    {
        private readonly Stopwatch _stopwatch;
        private int _counter;

        public FpsCounter()
        {
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
            _counter = 0;
        }

        public override void OnUpdate(float elapsed)
        {
            _counter++;
            var fps = _counter/((float) _stopwatch.ElapsedMilliseconds/1000);
            Console.WriteLine(fps);
        }

        public override void OnMessage(Message message)
        {
        }

        protected override void OnCreate()
        {
        }
    }
}