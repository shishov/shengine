﻿#region

using System;
using Microsoft.Xna.Framework;
using NetComponentEntitySystem;
using NetComponentEntitySystem.BaseSystems;
using ShEngine.EntitySystem.Components;
using ShEngineSamples.Components;

#endregion

namespace ShEngineSamples.Systems
{
    internal class MovementSystem : EntityProcessingSystem<Transformable2, Physics>
    {
        private readonly Vector2 _center1 = new Vector2(300, 300);
        private readonly Vector2 _center2 = new Vector2(600, 300);
        private readonly float _mass1 = 2000f;
        private readonly float _mass2 = 2000f;

        public override void Begin()
        {
        }

        public override void Process(Entity entity, Transformable2 component1, Physics component2)
        {
            var r1 = _center1 - component1.WorldPosition;
            var rlen1 = r1.Length();
            rlen1 = Math.Max(rlen1, 200f);
            r1.Normalize();

            var r2 = _center2 - component1.WorldPosition;
            var rlen2 = r2.Length();
            rlen2 = Math.Max(rlen2, 200f);
            r2.Normalize();

            var force1 = 6.67545f*component2.Mass*_mass1/(rlen1*rlen1);
            var force2 = 6.67545f*component2.Mass*_mass2/(rlen2*rlen2);
            component2.Velocity += component2.Mass*((force1*r1) + (force2*r2));
            component1.Move(component2.Velocity);
        }

        public override void End()
        {
        }
    }
}