﻿using System;
using Microsoft.Xna.Framework;
using NetComponentEntitySystem;
using NetComponentEntitySystem.BaseSystems;
using ShEngine.EntitySystem.Components.Volume;
using ShEngineSamples.Components;

namespace ShEngineSamples.Systems
{
    class RotationSystem : EntityProcessingSystem<Transform, Rotatable>
    {
        public override void Begin()
        {
        }

        public override void Process(Entity entity, Transform component1, Rotatable component2)
        {
            component1.Local *= Matrix.CreateRotationY(component2.RotationSpeed);
        }

        public override void End()
        {
        }
    }
}
