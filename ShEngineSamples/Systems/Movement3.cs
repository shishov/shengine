﻿#region

using System;
using Microsoft.Xna.Framework;
using NetComponentEntitySystem;
using NetComponentEntitySystem.BaseSystems;
using ShEngine.EntitySystem.Components.Volume;
using ShEngineSamples.Components;

#endregion

namespace ShEngineSamples.Systems
{
    internal class Movement3 : EntityProcessingSystem<Transform, Physics3>
    {
        private readonly Vector3 _center1 = new Vector3(-50, 0, 0);
        private readonly Vector3 _center2 = new Vector3(50, 0, 0);
        private readonly float _mass1 = 50f;
        private readonly float _mass2 = 50f;

        public override void Begin()
        {
        }

        public override void Process(Entity entity, Transform component1, Physics3 component2)
        {
            var r1 = _center1 - component1.World.Translation;
            var rlen1 = r1.Length();
            rlen1 = Math.Max(rlen1, 20f);
            r1.Normalize();

            var r2 = _center2 - component1.World.Translation;
            var rlen2 = r2.Length();
            rlen2 = Math.Max(rlen2, 20f);
            r2.Normalize();

            var force1 = 6.67545f*component2.Mass*_mass1/(rlen1*rlen1);
            var force2 = 6.67545f*component2.Mass*_mass2/(rlen2*rlen2);
            component2.Velocity += component2.Mass*((force1*r1) + (force2*r2));
            component1.Move(component2.Velocity);
            //component1.RotateX(component1.World.Translation.X/10000);
            //component1.Orientation *= Matrix.CreateScale(component1.World.Translation.Y/300 + 1);
        }

        public override void End()
        {
        }
    }
}