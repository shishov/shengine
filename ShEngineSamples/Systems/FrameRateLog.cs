﻿#region

using System;
using System.Diagnostics;
using NetComponentEntitySystem;
using NetComponentEntitySystem.BaseSystems;
using ShEngine.EntitySystem.Components;

#endregion

namespace ShEngineSamples.Systems
{
    internal class FrameRateLog : EntityProcessingSystem<Transformable2>
    {
        private readonly Stopwatch _stopwatch;
        private int _counter;

        public FrameRateLog()
        {
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
            _counter = 0;
        }

        public override void Begin()
        {
        }

        public override void Process(Entity entity, Transformable2 component)
        {
            _counter++;
            var fps = _counter/((float) _stopwatch.ElapsedMilliseconds/1000);
            Console.WriteLine(fps);
        }

        public override void End()
        {
        }
    }
}