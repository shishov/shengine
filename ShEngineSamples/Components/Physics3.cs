﻿#region

using Microsoft.Xna.Framework;
using NetComponentEntitySystem;

#endregion

namespace ShEngineSamples.Components
{
    internal class Physics3 : IComponent
    {
        public Physics3(Vector3 value)
        {
            Velocity = value;
            Mass = 0.1f;
        }

        public Vector3 Velocity { get; set; }
        public float Mass { get; set; }
    }
}