﻿#region

using Microsoft.Xna.Framework;
using NetComponentEntitySystem;

#endregion

namespace ShEngineSamples.Components
{
    internal class Physics : IComponent
    {
        public Physics(Vector2 value)
        {
            Velocity = value;
            Mass = 1f;
        }

        public Vector2 Velocity { get; set; }
        public float Mass { get; set; }
    }
}