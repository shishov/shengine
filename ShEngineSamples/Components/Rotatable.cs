﻿using NetComponentEntitySystem;

namespace ShEngineSamples.Components
{
    class Rotatable : IComponent
    {
        public float RotationSpeed;

        public Rotatable(float speed)
        {
            RotationSpeed = speed;
        }
    }
}
