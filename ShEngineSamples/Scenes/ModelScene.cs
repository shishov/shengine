﻿#region

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;
using ShEngine.Common;
using ShEngine.EntitySystem.Components.Volume;
using ShEngine.EntitySystem.Systems;
using ShEngineSamples.Components;
using ShEngineSamples.Systems;

#endregion

namespace ShEngineSamples.Scenes
{
    internal class ModelScene : Scene
    {
        private EntityWorld _world;

        public ModelScene(ShGame game, string name)
            : base(game, name)
        {
        }

        public override void Initialize()
        {
            _world = new EntityWorld();

            _world.AddSystem(new Basic3DRenderer(), ExecutionMode.Synchronous, GameLoopType.DrawLoop);
            _world.AddSystem(new Movement3(), ExecutionMode.Synchronous, GameLoopType.UpdateLoop);

            var model = AssetManager.GetXna<Model>("Models/barrel");

            for (var i = 0; i < 100; i++)
            {
                var entity = new Entity(_world);
                entity.AddComponent(new ModelComponent(model));
                entity.AddComponent(new Transform(new Vector3(0, (float) i/10, 0)));
                entity.AddComponent(new Physics3(new Vector3(0.2f, -0.1f, -0.1f)));
                _world.AddEntity(entity);
            }

            GameServices.GetService<ShGame>().Disposed += _world.Destroy;
        }

        public override void Draw(GameTime time)
        {
            _world.Draw(time.ElapsedGameTime.Milliseconds);
        }

        public override void Update(GameTime time)
        {
            _world.Update(time.ElapsedGameTime.Milliseconds);
        }
    }
}