﻿#region

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;
using ShEngine.Common;
using ShEngine.EntitySystem.Components;
using ShEngine.EntitySystem.Graphics.Plane;
using ShEngine.EntitySystem.Systems;
using ShEngineSamples.Components;
using ShEngineSamples.Systems;

#endregion

namespace ShEngineSamples.Scenes
{
    internal class DefaultScene : Scene
    {
        private EntityWorld _world;

        public DefaultScene(ShGame game, string name)
            : base(game, name)
        {
        }

        public override void Initialize()
        {
            var manager = GameServices.GetService<GraphicsDeviceManager>();
            manager.PreferredBackBufferWidth = 900;
            manager.PreferredBackBufferHeight = 600;

            _world = new EntityWorld();

            _world.AddSystem(new SpriteRenderingSystem(), ExecutionMode.Synchronous, GameLoopType.DrawLoop);
            _world.AddSystem(new MovementSystem(), ExecutionMode.ThreadedSync, GameLoopType.UpdateLoop);

            var texture = AssetManager.GetXna<Texture2D>("test");
            var r = new Random();
            for (var i = 0; i < 5000; i++)
            {
                var e = new Entity(_world);

                var t = new Transformable2();
                t.Move(new Vector2(450, (float) i/100 + 80));
                //e.AddComponent(t);

                e.AddComponent(new Drawable(new Sprite(texture, null)));
                var speed = new Vector2(2.5f, 0);
                e.AddComponent(new Physics(speed));
                //_world.AddEntity(e);
            }

            GameServices.GetService<ShGame>().Disposed += _world.Destroy;
        }

        public override void Draw(GameTime time)
        {
            _world.Draw(time.ElapsedGameTime.Milliseconds);
        }

        public override void Update(GameTime time)
        {
            _world.Update(time.ElapsedGameTime.Milliseconds);
        }
    }
}