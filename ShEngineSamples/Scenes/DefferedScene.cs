﻿#region

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NetComponentEntitySystem;
using ShEngine;
using ShEngine.Common;
using ShEngine.EntitySystem;
using ShEngine.EntitySystem.Components.Volume;
using ShEngine.EntitySystem.Scripts;
using ShEngine.EntitySystem.Systems;
using ShEngineSamples.Components;
using ShEngineSamples.Systems;
using DirectionalLight = ShEngine.EntitySystem.Components.Volume.DirectionalLight;

#endregion

namespace ShEngineSamples.Scenes
{
    internal class DefferedScene : Scene
    {
        private EntityWorld _world;

        public DefferedScene(ShGame game, string name)
            : base(game, name)
        {
        }

        public override void Initialize()
        {
            _world = new EntityWorld();
            var graphicsManager = GameServices.GetService<GraphicsDeviceManager>();
            graphicsManager.PreferredBackBufferWidth = 1600;
            graphicsManager.PreferredBackBufferHeight = 900;
            graphicsManager.SynchronizeWithVerticalRetrace = false;
            graphicsManager.ApplyChanges();
            Game.IsFixedTimeStep = false;

            var camera = new Entity(_world);
            camera.AddComponent<Transform>();
            camera.AddComponent(new Camera());
            camera.AddScript(new FreeCamera(new Vector3(10, 10, 10), Vector3.Zero));
            _world.AddEntity(camera);

            var graphics = GameServices.GetService<GraphicsDevice>();
            
            var light = new Entity(_world);
            light.AddComponent(new Transform(new Vector3(-8, 10, 0)));
            light.AddComponent(new PointLight(graphics, 500, Color.Orange.ToVector4(), 0.5f, true, 256));
            light.AddComponent(new Rotatable(0.004f));
            _world.AddEntity(light);

            var light1 = new Entity(_world);
            light1.AddComponent(new Transform(new Vector3(-20, 100, 0)));
            light1.AddComponent(new PointLight(graphics, 100, Color.Blue.ToVector4(), 1f, true, 256));
            light1.AddComponent(new Rotatable(0.008f));
            //_world.AddEntity(light1);
            
            var sun = new Entity(_world);
            sun.AddComponent(new DirectionalLight(new Vector3(-1, -1, 0), Color.Orange, 0.5f));
            _world.AddEntity(sun);


            //var flashlight = new Entity(_world);
            var t = new Transform(camera.GetComponent<Transform>())
            {
                Local = Matrix.CreateScale(1f/10f) * Matrix.CreateTranslation(new Vector3(0.5f, -0.5f, -1.5f))
            };
            //flashlight.AddComponent(t);
            //flashlight.AddComponent(new ModelComponent(AssetManager.GetXna<Model>("Models/flashlight")));
            //_world.AddEntity(flashlight);


            var cookie = AssetManager.GetXna<Texture2D>("Deffered/DefaultCookie");

            var light2 = new Entity(_world);
            var transform = new Transform(t)
            {
                Local = Matrix.CreateScale(100) * Matrix.CreateTranslation(new Vector3(0, 0, -2f))
            };
            light2.AddComponent(transform);
            light2.AddComponent(new SpotLight(graphics, Color.White.ToVector4(), 0.5f, true, 256, cookie, MathHelper.PiOver4));
            _world.AddEntity(light2);

            _world.AddSystem(new DefferedRenderer(), ExecutionMode.Synchronous, GameLoopType.DrawLoop);
            _world.AddSystem(new RotationSystem(), ExecutionMode.Synchronous, GameLoopType.UpdateLoop);
            
            var scene = new Entity(_world);
            var sceneModel = AssetManager.GetXna<Model>("Models/barrel");
            scene.AddComponent(new ModelComponent(sceneModel));
            scene.AddComponent(new Transform());
            _world.AddEntity(scene);

            GameServices.GetService<ShGame>().Disposed += _world.Destroy;
        }

        public override void Draw(GameTime time)
        {
            _world.Draw(time.ElapsedGameTime.Milliseconds);
        }

        public override void Update(GameTime time)
        {
            Mouse.SetPosition(0, 0);
            if (Input.IsNewKeyPress(Keys.Escape))
                Game.Exit();

            if (Input.IsNewKeyPress(Keys.F))
               GameServices.GetService<GraphicsDeviceManager>().ToggleFullScreen();

            _world.Update(time.ElapsedGameTime.Milliseconds);
        }
    }
}