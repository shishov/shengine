#region

using Microsoft.Xna.Framework;
using ShEngine.Common;
using ShEngineSamples.Scenes;

#endregion

namespace ShEngineSamples
{
    public class GameSample : ShGame
    {
        protected override void Initialize()
        {
            base.Initialize();

            var manager = GameServices.GetService<GraphicsDeviceManager>();
            manager.PreferredBackBufferWidth = 900;
            manager.PreferredBackBufferHeight = 600;

            //AddAndSetAvtice(new DefaultScene(this, "Default"));
            //AddAndSetAvtice(new ModelScene("Model"));
            AddAndSetAvtice(new DefferedScene(this, "Deffered"));
        }
    }
}