﻿#region

using System;
using Microsoft.Xna.Framework;

#endregion

namespace ShEngineSamples
{
    public static class ExtensionMethods
    {
        #region Methods

        /// <summary>
        ///     Rotates vector relative to (0,0)
        /// </summary>
        /// <param name="vec">Source vector</param>
        /// <param name="angle">Amount</param>
        /// <returns>Rotated vector</returns>
        public static Vector2 Rotate(this Vector2 vec, float angle)
        {
            Vector2 result;
            Rotate(ref vec, angle, out result);
            return result;
        }

        public static void Rotate(ref Vector2 vec, float angle, out Vector2 result)
        {
            result.X = vec.X*(float) Math.Cos(angle) - vec.Y*(float) Math.Sin(angle);
            result.Y = vec.X*(float) Math.Sin(angle) + vec.Y*(float) Math.Cos(angle);
        }

        #endregion
    }
}