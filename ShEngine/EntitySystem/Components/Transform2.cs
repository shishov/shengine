﻿#region

using Microsoft.Xna.Framework;
using NetComponentEntitySystem;
using ShEngine.EntitySystem.Graphics.Plane;

#endregion

namespace ShEngine.EntitySystem.Components
{
    /// <summary>
    ///     Simple 2D Transform component
    /// </summary>
    public class Transformable2 : IComponent
    {
        #region Fields

        private Transform2 _local;
        private Transformable2 _parent;

        #endregion

        #region Constructors

        public Transformable2()
        {
            _local = Transform2.CreateZero();
        }

        public Transformable2(Transformable2 parent)
            : this()
        {
            _parent = parent;
        }

        #endregion

        #region Properties

        public Transformable2 Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        public Transform2 World
        {
            get { return _parent == null ? _local : _local*_parent.World; }
        }

        public Transform2 Local
        {
            get { return _local; }
            set { _local = value; }
        }

        public Vector2 WorldPosition
        {
            get { return World.Position; }
        }

        public float WorldRotation
        {
            get { return World.Rotation; }
        }

        public Vector2 WorldScale
        {
            get { return World.Scale; }
        }

        public Vector2 LocalPosition
        {
            get { return Local.Position; }
            set { ApplyLocalTransform(Transform2.CreateTranslation(value - Local.Position)); }
        }

        public float LocalRotation
        {
            get { return Local.Rotation; }
            set { ApplyLocalTransform(Transform2.CreateRotation(value - Local.Rotation)); }
        }

        public Vector2 LocalScale
        {
            get { return Local.Scale; }
            set { ApplyLocalTransform(Transform2.CreateScale(value/Local.Scale)); }
        }

        #endregion

        #region Methods

        public void ApplyLocalTransform(Transform2 delta)
        {
            _local = _local*delta;
        }

        public void Move(Vector2 vec)
        {
            ApplyLocalTransform(Transform2.CreateTranslation(vec));
        }

        public void Rotate(float angle)
        {
            ApplyLocalTransform(Transform2.CreateRotation(angle));
        }

        public void Scale(float amount)
        {
            ApplyLocalTransform(Transform2.CreateScale(amount));
        }

        public void Scale(Vector2 amount)
        {
            ApplyLocalTransform(Transform2.CreateScale(amount));
        }

        #endregion
    }
}