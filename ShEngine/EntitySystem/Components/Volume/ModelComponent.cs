﻿#region

using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;

#endregion

namespace ShEngine.EntitySystem.Components.Volume
{
    public class ModelComponent : IComponent
    {
        public readonly Model Model;

        public ModelComponent(Model model)
        {
            Model = model;
        }
    }
}