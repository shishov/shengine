﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;

namespace ShEngine.EntitySystem.Components.Volume
{
    public class SpotLight : IComponent
    {
        private readonly float _farPlane;
        private readonly float _fov;
        private readonly float _nearPlane;
        private readonly Matrix _projection;
        private readonly RenderTarget2D _shadowMap;
        private readonly int _shadowMapResoloution;
        private Texture2D _attenuationTexture;
        private Vector4 _color;
        private float _depthBias;
        private float _intensity;
        private bool _isWithShadows;

        #region GetFunctions
        //Get Color 
        public Vector4 GetColor()
        {
            return _color;
        }

        //Get Intensity 
        public float GetIntensity()
        {
            return _intensity;
        }

        //Get NearPlane 
        public float GetNearPlane()
        {
            return _nearPlane;
        }

        //Get FarPlane 
        public float GetFarPlane()
        {
            return _farPlane;
        }

        //Get FOV 
        public float GetFov()
        {
            return _fov;
        }

        //Get IsWithShadows 
        public bool GetIsWithShadows()
        {
            return _isWithShadows;
        }

        //Get ShadowMapResoloution 
        public int GetShadowMapResoloution()
        {
            return _shadowMapResoloution < 2048 ? _shadowMapResoloution : 2048;
        }

        //Get DepthBias 
        public float GetDepthBias()
        {
            return _depthBias;
        }

        //Get Projection 
        public Matrix GetProjection()
        {
            return _projection;
        }

        //Get ShadowMap 
        public RenderTarget2D GetShadowMap()
        {
            return _shadowMap;
        }

        //Get Attenuation Texture 
        public Texture2D GetAttenuationTexture()
        {
            return _attenuationTexture;
        }

        #endregion

        #region Set Functions

        //Set Color 
        public void SetColor(Vector4 color)
        {
            _color = color;
        }

        //Set Color 
        public void SetColor(Color color)
        {
            _color = color.ToVector4();
        }

        //Set Intensity 
        public void SetIntensity(float intensity)
        {
            _intensity = intensity;
        }

        //Set isWithShadows 
        public void SetIsWithShadows(bool iswith)
        {
            _isWithShadows = iswith;
        }

        //Set DepthBias 
        public void SetDepthBias(float bias)
        {
            _depthBias = bias;
        }

        //Set Attenuation Texture 
        public void SetAttenuationTexture(Texture2D attenuationTexture)
        {
            _attenuationTexture = attenuationTexture;
        }

        #endregion

        //Constructor 
        public SpotLight(GraphicsDevice graphicsDevice, Vector4 color, float intensity,
            bool isWithShadows, int shadowMapResoloution,
            Texture2D attenuationTexture, float fov)
        {
            //Color 
            SetColor(color);

            //Intensity 
            SetIntensity(intensity);

            //NearPlane 
            _nearPlane = 0.1f;

            //FarPlane 
            _farPlane = 1000.0f;

            //FOV 
            _fov = fov;

            //Set whether Is With Shadows 
            SetIsWithShadows(isWithShadows);

            //Shadow Map Resoloution 
            _shadowMapResoloution = shadowMapResoloution;

            //Depth Bias 
            _depthBias = 1.0f / 2000.0f;
            
            //Projection 
            _projection = Matrix.CreatePerspectiveFieldOfView(_fov, 1.0f, _nearPlane,
                _farPlane);

            //Shadow Map 
            _shadowMap = new RenderTarget2D(graphicsDevice, GetShadowMapResoloution(),
                GetShadowMapResoloution(), false,
                SurfaceFormat.Single,
                DepthFormat.Depth24Stencil8);

            //Attenuation Texture 
            _attenuationTexture = attenuationTexture;
        }

        //Calculate the Cosine of the LightAngle 
        public float LightAngleCos()
        {
            return (float)Math.Cos(_fov);
        }
    }
}
