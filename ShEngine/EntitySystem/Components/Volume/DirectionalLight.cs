﻿using Microsoft.Xna.Framework;
using NetComponentEntitySystem;

namespace ShEngine.EntitySystem.Components.Volume
{
    public class DirectionalLight : IComponent
    {
        private Vector4 _color;
        private Vector3 _direction;
        private float _intensity;

        #region Get Functions

        //Get Direction 
        public Vector3 GetDirection()
        {
            return _direction;
        }

        //Get Color 
        public Vector4 GetColor()
        {
            return _color;
        }

        //Get Intensity 
        public float GetIntensity()
        {
            return _intensity;
        }

        #endregion

        #region Set Functions

        //Set Direction 
        public void SetDirection(Vector3 dir)
        {
            dir.Normalize();
            _direction = dir;
        }

        //Set Color 
        public void SetColor(Vector4 color)
        {
            _color = color;
        }

        //Set Color 
        public void SetColor(Color color)
        {
            _color = color.ToVector4();
        }

        //Set Intensity 
        public void SetIntensity(float intensity)
        {
            _intensity = intensity;
        }

        #endregion

        //Constructor 
        public DirectionalLight(Vector3 direction, Vector4 color, float intensity)
        {
            SetDirection(direction);
            SetColor(color);
            SetIntensity(intensity);
        }

        //Constructor 
        public DirectionalLight(Vector3 direction, Color color, float intensity)
        {
            SetDirection(direction);
            SetColor(color);
            SetIntensity(intensity);
        }
    }
}
