﻿#region

using Microsoft.Xna.Framework;
using NetComponentEntitySystem;

#endregion

namespace ShEngine.EntitySystem.Components.Volume
{
    public class Transform : IComponent
    {
        private readonly Transform _parent;
        public Matrix Local;

        public Transform()
        {
            Local = Matrix.Identity;
        }

        public Transform(Transform parent)
            : this()
        {
            _parent = parent;
        }

        public Transform(Vector3 translation)
            : this()
        {
            Local *= Matrix.CreateTranslation(translation);
        }

        public Matrix World
        {
            get { return _parent == null ? Local : Local * _parent.World; }
        }

        public Vector3 Position
        {
            get { return World.Translation; }
        }

        public void SetFromLocalView(Matrix value)
        {
            Local = Matrix.Invert(value);
        }

        public Matrix GetView()
        {
            return Matrix.Invert(World);
        }

        public void Move(Vector3 amount)
        {
            Local *= Matrix.CreateTranslation(amount);
        }
    }
}