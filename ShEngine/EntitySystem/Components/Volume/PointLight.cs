﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;

namespace ShEngine.EntitySystem.Components.Volume
{
    public class PointLight : IComponent
    {
        public bool CastShadows;
        public float Intensity;
        private readonly RenderTargetCube _shadowMap;
        private readonly int _shadowMapResoloution;
        public Vector4 Color;
        public float Radius;

        public PointLight(GraphicsDevice graphicsDevice, float radius,
            Vector4 color, float intensity, bool isWithShadows,
            int shadowMapResoloution)
        {
            Radius = radius;
            Color = color;
            Intensity = intensity;
            CastShadows = isWithShadows;
            _shadowMapResoloution = shadowMapResoloution;
            _shadowMap = new RenderTargetCube(graphicsDevice,GetShadowMapResolution(),false,SurfaceFormat.Single,DepthFormat.Depth24Stencil8);
        }
        
        public void SetRadius(float value)
        {
            Radius = value;
        }

        public float GetRadius()
        {
            return Radius;
        }

        public void SetColor(Vector4 value)
        {
            Color = value;
        }

        public void SetColor(Color color)
        {
            Color = color.ToVector4();
        }

        public Vector4 GetColor()
        {
            return Color;
        }

        public int GetShadowMapResolution()
        {
            return _shadowMapResoloution < 2048 ? _shadowMapResoloution : 2048;
        }

        public RenderTargetCube GetShadowMap()
        {
            return _shadowMap;
        }

        public float GetDepthBias()
        {
            return (1.0f / (20f * Radius));
        }
    }
}
