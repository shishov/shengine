﻿#region

using System;
using Microsoft.Xna.Framework;
using NetComponentEntitySystem;

#endregion

namespace ShEngine.EntitySystem.Components.Volume
{
    public class Camera : IComponent
    {
        public float AspectRatio = 16f / 9f;
        public float NearClip = 1;
        public float FarClip = 1000;
        public float Fov = MathHelper.PiOver2;

        public Matrix GetProjection()
        {
            return Matrix.CreatePerspectiveFieldOfView(Fov, AspectRatio, NearClip, FarClip);
        }

        //Calculate Frustum Corner of the Camera 
        public Vector3 GetViewFrustrum()
        {
            var cornerFrustum = Vector3.Zero;
            var thfov = (float) Math.Tan(Fov/2);
            cornerFrustum.Y = thfov * 1;
            cornerFrustum.X = thfov * AspectRatio;
            cornerFrustum.Z = 1;
            return cornerFrustum;
        }
    }
}