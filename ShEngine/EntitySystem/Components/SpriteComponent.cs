﻿#region

using NetComponentEntitySystem;
using ShEngine.EntitySystem.Graphics.Plane;

#endregion

namespace ShEngine.EntitySystem.Components
{
    public class Drawable : IComponent
    {
        public readonly Sprite Sprite;

        public Drawable(Sprite sprite)
        {
            Sprite = sprite;
        }
    }
}