﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using NetComponentEntitySystem;
using ShEngine.EntitySystem.Components.Volume;

namespace ShEngine.EntitySystem.Scripts
{
    public class FreeCamera : Script
    {
        private Vector3 _pos;
        private Vector3 _look;
        private float _dtx;
        private float _dty;

        public FreeCamera(Vector3 position, Vector3 lookAt)
        {
            _pos = position;
            _look = lookAt;
        }

        public override void OnUpdate(float elapsed)
        {
            var mov = new Vector3();
            var kbstate = Input.KeyboardState;

            if (kbstate.IsKeyDown(Keys.W))
                mov.Z += 1;
            if (kbstate.IsKeyDown(Keys.S))
                mov.Z -= 1;
            if (kbstate.IsKeyDown(Keys.A))
                mov.X += 1;
            if (kbstate.IsKeyDown(Keys.D))
                mov.X -= 1;
            if (kbstate.IsKeyDown(Keys.LeftShift))
                mov *= 3f;
            if (kbstate.IsKeyDown(Keys.LeftControl))
                mov /= 3f;

            mov /= 2f;
            var yaw = Input.Cursor.X  / _dtx;
            var pitch = Input.Cursor.Y / _dty;
            
            var camT = Entity.GetComponent<Transform>();
            camT.Local = Matrix.CreateTranslation(-mov) * Matrix.CreateFromYawPitchRoll(-yaw, -pitch, 0) * camT.Local;
        }

        public override void OnMessage(Message message)
        {
            throw new System.NotImplementedException();
        }

        protected override void OnCreate()
        {
            Input.ShowCursor = false;
            var cam = Entity.GetComponent<Camera>();
            var camT = Entity.GetComponent<Transform>();
            camT.Local = Matrix.Invert(Matrix.CreateLookAt(_pos, _look, Vector3.Up));
            cam.Fov = MathHelper.PiOver4;

            _dtx = 700;
            _dty = 700 * cam.AspectRatio;
        }
    }
}
