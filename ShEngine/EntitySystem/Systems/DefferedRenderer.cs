﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using NetComponentEntitySystem;
using NetComponentEntitySystem.Utilities;
using ShEngine.Common;
using ShEngine.EntitySystem.Components.Volume;
using ShEngine.EntitySystem.Graphics.Plane;
using ShEngine.EntitySystem.Graphics.Volume;
using ShEngine.Utitlities;
using Microsoft.Xna.Framework.Graphics;

namespace ShEngine.EntitySystem.Systems
{
    /// <summary>
    /// Advanced deffered renderer with exponential shadows and ssao
    /// </summary>
    public class DefferedRenderer : ComplexEntitySystem
    {
        /// <summary>
        /// System requirements
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, Aspect<Type>> GetAspects()
        {
            var aspects = new Dictionary<string, Aspect<Type>>
            {
                { "Models", new Aspect<Type>().AllOf(typeof(Transform), typeof(ModelComponent)) },
                { "DirectionalLights", new Aspect<Type>().AllOf(typeof(Components.Volume.DirectionalLight)) },
                { "PointLights", new Aspect<Type>().AllOf(typeof(Transform), typeof(PointLight)) },
                { "SpotLights", new Aspect<Type>().AllOf(typeof(Transform), typeof(SpotLight)) },
                { "Cameras", new Aspect<Type>().AllOf(typeof(Transform), typeof(Camera)) },
            };
            return aspects;
        }

        // Effects
        readonly Effect _clear;
        readonly Effect _gBuffer;
        readonly Effect _directionalLight;
        readonly Effect _pointLight;
        readonly Effect _spotLight;
        readonly Effect _compose;
        readonly Effect _depthWriter;

        readonly BlendState _lightMapBs;
        readonly RenderTargetBinding[] _gBufferTargets;
        readonly Vector2 _gBufferTextureSize;
        readonly RenderTarget2D _lightMap;
        readonly FullscreenQuad _fsq;
        readonly Model _pointLightGeometry;
        readonly Model _spotLightGeometry;
        readonly GraphicsDevice _graphics;
        readonly SpriteFont _debugFont;
        readonly Stopwatch _stopwatch;

        readonly SpriteBatch _batch;
        readonly RenderTarget2D _sceneTexture;
        private int _frames;
        private int _fps;
        private object _lock;


        public RenderTargetBinding[] GetGBuffer()
        {
            return _gBufferTargets;
        }

        public DefferedRenderer()
            : base(GetAspects())
        {
            var manager = GameServices.GetService<GraphicsDeviceManager>();
            _graphics = GameServices.GetService<GraphicsDevice>();
            _batch = GameServices.GetService<SpriteBatch>();
            _debugFont = AssetManager.GetXna<SpriteFont>("Deffered/DebugFont");
            _lock = AssetManager.Get<Object>("DrawLock");

            var width = manager.PreferredBackBufferWidth;
            var height = manager.PreferredBackBufferWidth;
            
            //Load Clear Shader 
            _clear = AssetManager.GetXna<Effect>("Deffered/Clear");
            _clear.CurrentTechnique = _clear.Techniques[0];

            //Load GBuffer Shader 
            _gBuffer = AssetManager.GetXna<Effect>("Deffered/GBuffer");
            _gBuffer.CurrentTechnique = _gBuffer.Techniques[0];

            //Load Directional Light Shader 
            _directionalLight = AssetManager.GetXna<Effect>("Deffered/DirectionalLight");
            _directionalLight.CurrentTechnique = _directionalLight.Techniques[0];

            //Load Point Light Shader 
            _pointLight = AssetManager.GetXna<Effect>("Deffered/PointLight");
            _pointLight.CurrentTechnique = _pointLight.Techniques[0];

            //Load Spot Light Shader 
            _spotLight = AssetManager.GetXna<Effect>("Deffered/SpotLight");
            _spotLight.CurrentTechnique = _spotLight.Techniques[0];

            //Load Composition Shader 
            _compose = AssetManager.GetXna<Effect>("Deffered/Composition");
            _compose.CurrentTechnique = _compose.Techniques[0];
            

            //Create LightMap BlendState 
            _lightMapBs = new BlendState
            {
                ColorSourceBlend = Blend.One,
                ColorDestinationBlend = Blend.One,
                ColorBlendFunction = BlendFunction.Add,
                AlphaSourceBlend = Blend.One,
                AlphaDestinationBlend = Blend.One,
                AlphaBlendFunction = BlendFunction.Add
            };

            //Set GBuffer Texture Size 
            _gBufferTextureSize = new Vector2(width, height);

            //Initialize GBuffer Targets Array 
            _gBufferTargets = new RenderTargetBinding[3];

            //Intialize Each Target of the GBuffer 
            _gBufferTargets[0] = new RenderTargetBinding(new RenderTarget2D(_graphics, width, height, false, SurfaceFormat.Rgba64, DepthFormat.Depth24Stencil8));
            _gBufferTargets[1] = new RenderTargetBinding(new RenderTarget2D(_graphics, width, height, false, SurfaceFormat.Rgba64, DepthFormat.Depth24Stencil8));
            _gBufferTargets[2] = new RenderTargetBinding(new RenderTarget2D(_graphics, width, height, false, SurfaceFormat.Vector2, DepthFormat.Depth24Stencil8));

            //Initialize LightMap 
            _lightMap = new RenderTarget2D(_graphics, width, height, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8);

            //Create Fullscreen Quad 
            _fsq = new FullscreenQuad(_graphics);

            //Load Point Light Geometry 
            _pointLightGeometry = AssetManager.GetXna<Model>("Deffered/Sphere");

            //Load Spot Light Geometry 
            _spotLightGeometry = AssetManager.GetXna<Model>("Deffered/Sphere");
            
            //Load the Depth Writing Shader 
            _depthWriter = AssetManager.GetXna<Effect>("Deffered/DepthWriter");
            _depthWriter.CurrentTechnique = _depthWriter.Techniques[0];
            
            //Create Scene Render Target 
            _sceneTexture = new RenderTarget2D(_graphics, width, height, false, SurfaceFormat.Color, DepthFormat.Depth24Stencil8);

            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        // Checked
        public override void Update()
        {
            if (Entities["Cameras"].Count <= 0) throw new Exception("Camera Required");
            var camera = Entities["Cameras"][0].GetComponent<Camera>();
            var cameraTransform = Entities["Cameras"][0].GetComponent<Transform>();
            
            DrawShadowMaps();
            DefferedDraw(camera, cameraTransform, null);
            DefferedDebug(_batch, camera, cameraTransform);
        }

        // Checked
        void DefferedDraw(Camera camera, Transform cameraTransform, RenderTarget2D output)
        {
            //Set States 
            _graphics.BlendState = BlendState.Opaque;
            _graphics.DepthStencilState = DepthStencilState.Default;
            _graphics.RasterizerState = RasterizerState.CullCounterClockwise;

            //Clear GBuffer 
            ClearGBuffer();

            //Make GBuffer 
            MakeGBuffer(camera, cameraTransform);

            //Make LightMap 
            MakeLightMap(camera, cameraTransform);

            //Make Final Rendered Scene 
            MakeFinal(output);
        }

        //GBuffer Creation 
        void MakeGBuffer(Camera camera, Transform cameraTransform)
        {
            //Set Depth State 
            _graphics.DepthStencilState = DepthStencilState.Default;

            //Set up global GBuffer parameters 
            _gBuffer.Parameters["View"].SetValue(cameraTransform.GetView());
            _gBuffer.Parameters["Projection"].SetValue(camera.GetProjection());

            //Draw Each Model 
            foreach (var e in Entities["Models"])
            {
                var model = e.GetComponent<ModelComponent>().Model;
                var transform = e.GetComponent<Transform>().World;

                //Draw Each ModelMesh 
                foreach (var mesh in model.Meshes)
                {
                    var world = mesh.ParentBone.Transform * transform;

                    //Draw Each ModelMeshPart 
                    foreach (var part in mesh.MeshParts)
                    {
                        //Set Vertex Buffer 
                        _graphics.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);

                        //Set Index Buffer 
                        _graphics.Indices = part.IndexBuffer;

                        //Set World 
                        _gBuffer.Parameters["World"].SetValue(world);

                        //Set WorldIT 
                        _gBuffer.Parameters["WorldViewIT"].SetValue(Matrix.Transpose(Matrix.Invert(world * cameraTransform.GetView())));
                        
                        //Set Albedo Texture 
                        _gBuffer.Parameters["Texture"].SetValue(part.Effect.Parameters["Texture"].GetValueTexture2D());

                        //Set Normal Texture 
                        _gBuffer.Parameters["NormalMap"].SetValue(part.Effect.Parameters["NormalMap"].GetValueTexture2D());

                        //Set Specular Texture 
                        _gBuffer.Parameters["SpecularMap"].SetValue(part.Effect.Parameters["SpecularMap"].GetValueTexture2D());

                        //Apply Effect 
                        _gBuffer.CurrentTechnique.Passes[0].Apply();

                        //Draw 
                        _graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                        part.NumVertices, part.StartIndex, part.PrimitiveCount);
                    }
                }
            }

            //Set RenderTargets off 
            _graphics.SetRenderTargets(null);
        }

        //Clear GBuffer 
        void ClearGBuffer()
        {
            //Set to ReadOnly depth for now... 
            _graphics.DepthStencilState = DepthStencilState.DepthRead;

            //Set GBuffer Render Targets 
            _graphics.SetRenderTargets(_gBufferTargets);

            //Set Clear Effect
            _clear.CurrentTechnique.Passes[0].Apply();

            //Draw 
            _fsq.Draw(_graphics);
        }

        //Debug 
        void DefferedDebug(SpriteBatch spriteBatch, Camera cam, Transform cameraTransform)
        {
            //Begin SpriteBatch 
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.PointClamp, null, RasterizerState.CullCounterClockwise);

            //Width + Height 
            const int width = 256;
            const int height = 256;

            //Set up Drawing Rectangle 
            var rect = new Rectangle(0, 0, width, height);

            //Draw GBuffer 0 
            spriteBatch.Draw((Texture2D)_gBufferTargets[0].RenderTarget, rect, Color.White);

            //Draw GBuffer 1 
            rect.X += width;
            spriteBatch.Draw((Texture2D)_gBufferTargets[1].RenderTarget, rect, Color.White);

            //Draw GBuffer 2 
            rect.X += width;
            spriteBatch.Draw((Texture2D)_gBufferTargets[2].RenderTarget, rect, Color.White);

            //Draw GBuffer 2 
            rect.X += width;
            var light = Entities["PointLights"][0].GetComponent<PointLight>();
            var map = (TextureCube) light.GetShadowMap();
            var w = light.GetShadowMapResolution();

            var data = new byte[w * w * 4];
            map.GetData(CubeMapFace.NegativeY, data);
            var tex = new Texture2D(_graphics, w, w);
            tex.SetData(data);

            spriteBatch.Draw(tex, rect, Color.White);

            rect.X += width;
            var spot = Entities["SpotLights"][0].GetComponent<SpotLight>().GetShadowMap();
            spriteBatch.Draw(spot, rect, Color.White);

            spriteBatch.End();
            spriteBatch.Begin();

            var pos = new Vector2(0, 170);
            spriteBatch.DrawString(_debugFont, String.Format("Camera: {0}", cameraTransform.Position), pos, Color.Gray);
            pos.Y += 15;
            spriteBatch.DrawString(_debugFont, String.Format("Models: {0}",Entities["Models"].Count), pos, Color.Gray);
            pos.Y += 15;
            spriteBatch.DrawString(_debugFont, String.Format("Cameras: {0}",Entities["Cameras"].Count), pos, Color.Gray);
            pos.Y += 15;
            spriteBatch.DrawString(_debugFont, String.Format("DirectionalLights: {0}",Entities["DirectionalLights"].Count), pos, Color.Gray);
            pos.Y += 15;
            spriteBatch.DrawString(_debugFont, String.Format("PointLights: {0}",Entities["PointLights"].Count), pos, Color.Gray);
            pos.Y += 15;
            spriteBatch.DrawString(_debugFont, String.Format("SpotLights: {0}", Entities["SpotLights"].Count), pos, Color.Gray);
            pos.Y += 15;


            _frames++;

            if (_stopwatch.ElapsedMilliseconds > 1000)
            {
                _fps = _frames;
                _frames = 0;
                _stopwatch.Restart();
            }

            spriteBatch.DrawString(_debugFont, String.Format("FPS: {0}", _fps), pos, Color.Gray);

            //End SpriteBatch 
            spriteBatch.End();
        }

        //Light Map Creation 
        void MakeLightMap(Camera camera, Transform cameraTransform)
        {
            //Set LightMap Target 
            _graphics.SetRenderTarget(_lightMap);

            //Clear to Transperant Black 
            _graphics.Clear(Color.Transparent);

            //Set States 
            _graphics.BlendState = _lightMapBs;
            _graphics.DepthStencilState = DepthStencilState.DepthRead;

            #region Set Global Samplers
            //GBuffer 1 Sampler 
            _graphics.Textures[0] = _gBufferTargets[0].RenderTarget;
            _graphics.SamplerStates[0] = SamplerState.LinearClamp;

            //GBuffer 2 Sampler 
            _graphics.Textures[1] = _gBufferTargets[1].RenderTarget;
            _graphics.SamplerStates[1] = SamplerState.LinearClamp;

            //GBuffer 3 Sampler 
            _graphics.Textures[2] = _gBufferTargets[2].RenderTarget;
            _graphics.SamplerStates[2] = SamplerState.PointClamp;

            //SpotLight Cookie Sampler 
            _graphics.SamplerStates[3] = SamplerState.LinearClamp;

            //ShadowMap Sampler 
            _graphics.SamplerStates[4] = SamplerState.PointClamp;
            #endregion


            //keyword(ololo1) << ololos;
            //keyword(ololo2) << ololod;
            //keyword(ololo3) << ololof;
            //[18:32:42] Митя Гладкий: keyword(pr::error,"enter");
            //keyword(ololo4) << ololog;

            //Calculate InverseView 
            var inverseView = Matrix.Invert(cameraTransform.GetView());

            //Calculate InverseViewProjection 
            var inverseViewProjection = Matrix.Invert(cameraTransform.GetView() * camera.GetProjection());

            //Set Directional Lights Globals 
            _directionalLight.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
            _directionalLight.Parameters["inverseView"].SetValue(inverseView);
            _directionalLight.Parameters["CameraPosition"].SetValue(cameraTransform.Position);
            _directionalLight.Parameters["GBufferTextureSize"].SetValue(_gBufferTextureSize);

            //Set the Directional Lights Geometry Buffers 
            _fsq.ReadyBuffers(_graphics);

            //Draw Directional Lights 
            foreach (var lightEntity in Entities["DirectionalLights"])
            {
                var light = lightEntity.GetComponent<Components.Volume.DirectionalLight>();

                //Set Directional Light Parameters 
                _directionalLight.Parameters["L"].SetValue(Vector3.Normalize(light.GetDirection()));
                _directionalLight.Parameters["LightColor"].SetValue(light.GetColor());
                _directionalLight.Parameters["LightIntensity"].SetValue(light.GetIntensity());

                //Apply 
                _directionalLight.CurrentTechnique.Passes[0].Apply();

                //Draw 
                _fsq.JustDraw(_graphics);
            }

            //Set Spot Lights Globals 
            _spotLight.Parameters["View"].SetValue(cameraTransform.GetView());
            _spotLight.Parameters["inverseView"].SetValue(inverseView);
            _spotLight.Parameters["Projection"].SetValue(camera.GetProjection());
            _spotLight.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
            _spotLight.Parameters["CameraPosition"].SetValue(cameraTransform.Position);
            _spotLight.Parameters["GBufferTextureSize"].SetValue(_gBufferTextureSize);

            //Set Spot Lights Geometry Buffers 
            _graphics.SetVertexBuffer(_spotLightGeometry.Meshes[0].MeshParts[0].VertexBuffer,
            _spotLightGeometry.Meshes[0].MeshParts[0].VertexOffset);
            _graphics.Indices = _spotLightGeometry.Meshes[0].MeshParts[0].IndexBuffer;

            //Draw Spot Lights 
            foreach (var lightEntity in Entities["SpotLights"])
            {
                var light = lightEntity.GetComponent<SpotLight>();
                var transform = lightEntity.GetComponent<Transform>();
                var world = transform.World;
                var pos = world.Translation;
                var direction = world.Forward;

                //Set Attenuation Cookie Texture and SamplerState 
                _graphics.Textures[3] = light.GetAttenuationTexture();

                //Set ShadowMap and SamplerState 
                _graphics.Textures[4] = light.GetShadowMap();

                //Set Spot Light Parameters 
                _spotLight.Parameters["World"].SetValue(world);
                _spotLight.Parameters["LightViewProjection"].SetValue(transform.GetView() * light.GetProjection());
                _spotLight.Parameters["LightPosition"].SetValue(pos); 
                _spotLight.Parameters["LightColor"].SetValue(light.GetColor());
                _spotLight.Parameters["LightIntensity"].SetValue(light.GetIntensity());
                _spotLight.Parameters["S"].SetValue(direction);
                _spotLight.Parameters["LightAngleCos"].SetValue(light.LightAngleCos());
                _spotLight.Parameters["LightHeight"].SetValue(light.GetFarPlane());
                _spotLight.Parameters["Shadows"].SetValue(light.GetIsWithShadows());
                _spotLight.Parameters["shadowMapSize"].SetValue(light.GetShadowMapResoloution());
                _spotLight.Parameters["DepthPrecision"].SetValue(light.GetFarPlane());
                _spotLight.Parameters["DepthBias"].SetValue(light.GetDepthBias());

                #region Set Cull Mode
                //Calculate L 
                var l = cameraTransform.Position - pos;

                //Calculate S.L 
                var sl = Math.Abs(Vector3.Dot(l, direction));

                //Check if SL is within the LightAngle, if so then draw the BackFaces, if not 
                //then draw the FrontFaces 
                _graphics.RasterizerState = sl < light.LightAngleCos() ? RasterizerState.CullCounterClockwise : RasterizerState.CullClockwise;
                #endregion

                //Apply 
                _spotLight.CurrentTechnique.Passes[0].Apply();

                //Draw 
                _graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                _spotLightGeometry.Meshes[0].MeshParts[0].NumVertices,
                _spotLightGeometry.Meshes[0].MeshParts[0].StartIndex,
                _spotLightGeometry.Meshes[0].MeshParts[0].PrimitiveCount);
            }


            //Set Point Lights Geometry Buffers 
            _graphics.SetVertexBuffer(_pointLightGeometry.Meshes[0].MeshParts[0].VertexBuffer,
            _pointLightGeometry.Meshes[0].MeshParts[0].VertexOffset);
            _graphics.Indices = _pointLightGeometry.Meshes[0].MeshParts[0].IndexBuffer;

            //Set Point Lights Globals 
            _pointLight.Parameters["inverseView"].SetValue(inverseView);
            _pointLight.Parameters["View"].SetValue(cameraTransform.GetView());
            _pointLight.Parameters["Projection"].SetValue(camera.GetProjection());
            _pointLight.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
            _pointLight.Parameters["CameraPosition"].SetValue(cameraTransform.Position);
            _pointLight.Parameters["GBufferTextureSize"].SetValue(_gBufferTextureSize);

            //Draw Point Lights without Shadows 
            foreach (var lightEntity in Entities["PointLights"])
            {
                var light = lightEntity.GetComponent<PointLight>();
                var transform = lightEntity.GetComponent<Transform>();

                //Set Point Light Sampler 
                _graphics.Textures[4] = light.GetShadowMap();
                _graphics.SamplerStates[4] = SamplerState.PointWrap;

                //Set Point Light Parameters 
                // TODO: FIX?
                var pos = transform.World.Translation;
                var scaledWorld = Matrix.Multiply(Matrix.CreateScale(light.GetRadius()), transform.World);
                _pointLight.Parameters["World"].SetValue(scaledWorld);
                _pointLight.Parameters["LightPosition"].SetValue(pos);
                _pointLight.Parameters["LightRadius"].SetValue(light.GetRadius());
                _pointLight.Parameters["LightColor"].SetValue(light.GetColor());
                _pointLight.Parameters["LightIntensity"].SetValue(light.Intensity);
                _pointLight.Parameters["Shadows"].SetValue(light.CastShadows);
                _pointLight.Parameters["DepthPrecision"].SetValue(light.GetRadius());
                _pointLight.Parameters["DepthBias"].SetValue(light.GetDepthBias());
                _pointLight.Parameters["shadowMapSize"].SetValue(light.GetShadowMapResolution());

                //Set Cull Mode 
                var diff = cameraTransform.Position - pos;

                var cameraToLight = (float)Math.Sqrt(Vector3.Dot(diff, diff)) / 100.0f;

                //If the Camera is in the light, render the backfaces, if it's out of the 
                //light, render the frontfaces 
                if (cameraToLight <= light.GetRadius())
                    _graphics.RasterizerState = RasterizerState.CullClockwise;
                else
                    _graphics.RasterizerState = RasterizerState.CullCounterClockwise;

                //Apply 
                _pointLight.CurrentTechnique.Passes[0].Apply();

                //Draw 
                _graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                _pointLightGeometry.Meshes[0].MeshParts[0].NumVertices,
                _pointLightGeometry.Meshes[0].MeshParts[0].StartIndex,
                _pointLightGeometry.Meshes[0].MeshParts[0].PrimitiveCount);
            }


            //Set States Off 
            _graphics.BlendState = BlendState.Opaque;
            _graphics.RasterizerState = RasterizerState.CullCounterClockwise;
            _graphics.DepthStencilState = DepthStencilState.Default;
        }

        //Composition 
        void MakeFinal(RenderTarget2D output)
        {
            //Set Composition Target 
            _graphics.SetRenderTarget(output);

            //Clear 
            _graphics.Clear(Color.Transparent);

            //Set Textures 
            _graphics.Textures[0] = _gBufferTargets[0].RenderTarget;
            _graphics.SamplerStates[0] = SamplerState.LinearClamp;

            _graphics.Textures[1] = _lightMap;
            _graphics.SamplerStates[1] = SamplerState.LinearClamp;

            //Set Effect Parameters 
            _compose.Parameters["GBufferTextureSize"].SetValue(_gBufferTextureSize);

            //Apply 
            _compose.CurrentTechnique.Passes[0].Apply();

            //Draw 
            _fsq.Draw(_graphics);
        }

        #region LightManager
        //Draw Shadow Maps 
        public void DrawShadowMaps()
        {
            //Set States 
            _graphics.BlendState = BlendState.Opaque;
            _graphics.DepthStencilState = DepthStencilState.Default;
            _graphics.RasterizerState = RasterizerState.CullCounterClockwise;

            //Foreach SpotLight with Shadows 
            foreach (var lightEntity in Entities["SpotLights"])
            {
                var light = lightEntity.GetComponent<SpotLight>();
                var transform = lightEntity.GetComponent<Transform>();

                //Draw it's Shadow Map 
                if (light.GetIsWithShadows())
                    DrawShadowMap(light, transform);
            }

            //Foreach PointLight with Shadows 
            foreach (var lightEntity in Entities["PointLights"])
            {
                var light = lightEntity.GetComponent<PointLight>();
                var transform = lightEntity.GetComponent<Transform>();

                //Draw it's Shadow Map 
                if (light.CastShadows) 
                    DrawShadowMap(light, transform);
            }
        }

        //Draw a Shadow Map for a Spot Light 
        private void DrawShadowMap(SpotLight light, Transform transform)
        {
            //Set Light's Target onto the Graphics Device 
            _graphics.SetRenderTarget(light.GetShadowMap());

            //Clear Target 
            _graphics.Clear(Color.Transparent);

            //Set global Effect parameters 
            _depthWriter.Parameters["View"].SetValue(transform.GetView());
            _depthWriter.Parameters["Projection"].SetValue(light.GetProjection());
            _depthWriter.Parameters["LightPosition"].SetValue(transform.World.Translation);
            _depthWriter.Parameters["DepthPrecision"].SetValue(light.GetFarPlane());

            //Draw Models 
            DrawModels();
        }

        //Draw a Shadow Map for a Point Light 
        private void DrawShadowMap(PointLight light, Transform transform)
        {
            //Initialize View Matrices Array 
            var views = new Matrix[6];
            var pos = transform.World.Translation;

            //Create View Matrices 
            views[0] = Matrix.CreateLookAt(pos, pos + Vector3.Forward, Vector3.Up);
            views[1] = Matrix.CreateLookAt(pos, pos + Vector3.Backward, Vector3.Up);
            views[2] = Matrix.CreateLookAt(pos, pos + Vector3.Left, Vector3.Up);
            views[3] = Matrix.CreateLookAt(pos, pos + Vector3.Right, Vector3.Up);
            views[4] = Matrix.CreateLookAt(pos, pos + Vector3.Down, Vector3.Forward);
            views[5] = Matrix.CreateLookAt(pos, pos + Vector3.Up, Vector3.Backward);

            //Create Projection Matrix 
            var projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90.0f),
                1.0f, 1.0f, light.GetRadius());

            //Set Global Effect Values 
            _depthWriter.Parameters["Projection"].SetValue(projection);
            _depthWriter.Parameters["LightPosition"].SetValue(pos);
            _depthWriter.Parameters["DepthPrecision"].SetValue(light.GetRadius());

            #region Forward

            _graphics.SetRenderTarget(light.GetShadowMap(), CubeMapFace.PositiveZ);

            //Clear Target 
            _graphics.Clear(Color.Transparent);

            //Set global Effect parameters 
            _depthWriter.Parameters["View"].SetValue(views[0]);

            //Draw Models 
            DrawModels();

            #endregion

            #region Backward

            _graphics.SetRenderTarget(light.GetShadowMap(), CubeMapFace.NegativeZ);

            //Clear Target 
            _graphics.Clear(Color.Transparent);

            //Set global Effect parameters 
            _depthWriter.Parameters["View"].SetValue(views[1]);

            //Draw Models 
            DrawModels();

            #endregion

            #region Left

            _graphics.SetRenderTarget(light.GetShadowMap(), CubeMapFace.NegativeX);

            //Clear Target 
            _graphics.Clear(Color.Transparent);

            //Set global Effect parameters 
            _depthWriter.Parameters["View"].SetValue(views[2]);

            //Draw Models 
            DrawModels();

            #endregion

            #region Right

            _graphics.SetRenderTarget(light.GetShadowMap(), CubeMapFace.PositiveX);

            //Clear Target 
            _graphics.Clear(Color.Transparent);

            //Set global Effect parameters 
            _depthWriter.Parameters["View"].SetValue(views[3]);

            //Draw Models 
            DrawModels();

            #endregion

            #region Down

            _graphics.SetRenderTarget(light.GetShadowMap(), CubeMapFace.NegativeY);

            //Clear Target 
            _graphics.Clear(Color.Transparent);

            //Set global Effect parameters 
            _depthWriter.Parameters["View"].SetValue(views[4]);

            //Draw Models 
            DrawModels();

            #endregion

            #region Up

            _graphics.SetRenderTarget(light.GetShadowMap(), CubeMapFace.PositiveY);

            //Clear Target 
            _graphics.Clear(Color.Transparent);

            //Set global Effect parameters 
            _depthWriter.Parameters["View"].SetValue(views[5]);

            //Draw Models 
            DrawModels();

            #endregion
        }

        //Draw Models 
        private void DrawModels()
        {
            //Draw Each Model 
            foreach (var entity in Entities["Models"])
            {
                var model = entity.GetComponent<ModelComponent>().Model;
                var transform = entity.GetComponent<Transform>().World;

                //Draw Each ModelMesh 
                foreach (var mesh in model.Meshes)
                {
                    var world = mesh.ParentBone.Transform * transform;

                    //Draw Each ModelMeshPart 
                    foreach (var part in mesh.MeshParts)
                    {
                        //Set Vertex Buffer 
                        _graphics.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);

                        //Set Index Buffer 
                        _graphics.Indices = part.IndexBuffer;

                        //Set World 
                        _depthWriter.Parameters["World"].SetValue(world);

                        //Apply Effect 
                        _depthWriter.CurrentTechnique.Passes[0].Apply();

                        //Draw 
                        _graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0,
                            part.NumVertices, part.StartIndex,
                            part.PrimitiveCount);
                    }
                }
            }
        }

        #endregion
    }
}
