﻿#region

using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;
using NetComponentEntitySystem.BaseSystems;
using ShEngine.Common;
using ShEngine.EntitySystem.Components.Volume;

#endregion

namespace ShEngine.EntitySystem.Systems
{
    public class Basic3DRenderer : EntityProcessingSystem<Transform, ModelComponent>
    {
        private readonly float _aspectRatio;

        public Basic3DRenderer()
        {
            var graphics = GameServices.GetService<GraphicsDeviceManager>();
            _aspectRatio = (float)graphics.PreferredBackBufferWidth / graphics.PreferredBackBufferHeight;
        }

        public override void Begin()
        {
        }

        public override void Process(Entity entity, Transform transform, ModelComponent model)
        {
            foreach (var mesh in model.Model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects.OfType<BasicEffect>())
                {
                    effect.EnableDefaultLighting();
                    effect.DirectionalLight0.Enabled = true;
                    effect.LightingEnabled = true;
                    effect.World = transform.World;
                    effect.View = Matrix.CreateLookAt(new Vector3(20, 20, 20), Vector3.Zero, Vector3.Up);
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45.0f), _aspectRatio, 1.0f, 10000.0f);
                }
                mesh.Draw();
            }
        }

        public override void End()
        {
        }
    }
}