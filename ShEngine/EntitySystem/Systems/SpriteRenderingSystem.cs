﻿#region

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;
using NetComponentEntitySystem.BaseSystems;
using ShEngine.Common;
using ShEngine.EntitySystem.Components;

#endregion

namespace ShEngine.EntitySystem.Systems
{
    public class SpriteRenderingSystem : EntityProcessingSystem<Transformable2, Drawable>
    {
        private readonly SpriteBatch _batch;

        public SpriteRenderingSystem()
        {
            _batch = GameServices.GetService<SpriteBatch>();
        }

        public override void Begin()
        {
            _batch.Begin();
        }

        public override void Process(Entity entity, Transformable2 transformation, Drawable sprite)
        {
            sprite.Sprite.Draw(_batch, transformation.WorldPosition, Color.White, Vector2.Zero, 0, transformation.WorldRotation, transformation.WorldScale.X);
        }

        public override void End()
        {
            _batch.End();
        }
    }
}