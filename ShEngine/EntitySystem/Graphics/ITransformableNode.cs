﻿#region

using System.Collections.Generic;

#endregion

namespace ShEngine.EntitySystem.Graphics
{
    /// <summary>
    ///     Main interface for all nodes
    /// </summary>
    /// <typeparam name="TTransform">Transformable component (Matrix for example)</typeparam>
    public interface ITransformableNode<TTransform>
    {
        #region Properties

        string Name { get; }
        List<ITransformableNode<TTransform>> ChildNodes { get; }
        ITransformableNode<TTransform> Parent { get; }
        TTransform World { get; }
        TTransform Local { get; }

        #endregion

        #region Methods

        ITransformableNode<TTransform> CreateChild(string name = "");
        void ApplyLocalTransform(TTransform delta);
        void UpdateWorld();

        #endregion
    }
}