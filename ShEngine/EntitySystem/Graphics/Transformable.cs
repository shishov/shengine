﻿namespace ShEngine.EntitySystem.Graphics
{
    /// <summary>
    ///     Class that provides "Attach To Node" method, must be inherited
    /// </summary>
    public abstract class Transformable<TNodeType, TTransform> where TNodeType : ITransformableNode<TTransform>
    {
        #region Fields

        public readonly string Name;

        #endregion

        #region Constructors

        internal Transformable(string name = "")
        {
            Name = name;
        }

        #endregion

        #region Properties

        public TNodeType Node { get; private set; }

        #endregion

        #region Methods

        public void AttachTo(TNodeType node)
        {
            Node = node;
        }

        #endregion
    }
}