﻿#region

using System.Collections.Generic;

#endregion

namespace ShEngine.EntitySystem.Graphics.Plane
{
    public class SpriteAnimation
    {
        #region Fields

        public readonly float FrameRate;
        public readonly bool Repeat;
        public readonly List<Sprite> Sprites;

        private float _currentFrameTime;
        private int _currentSpriteIndex;

        #endregion

        #region Constructors

        public SpriteAnimation(List<Sprite> sprites, float framerate, bool repeat)
        {
            Repeat = repeat;
            Sprites = sprites;
            FrameRate = framerate;
        }

        #endregion

        #region Properties

        public Sprite CurrentSprite
        {
            get { return Sprites[_currentSpriteIndex]; }
        }

        #endregion

        #region Methods

        public void Step(float dt)
        {
            _currentFrameTime += dt;
            if (_currentFrameTime > FrameRate)
            {
                _currentSpriteIndex += 1;
                if (_currentSpriteIndex >= Sprites.Count)
                    _currentSpriteIndex = 0;

                _currentFrameTime = 0;
            }
        }

        #endregion
    }
}