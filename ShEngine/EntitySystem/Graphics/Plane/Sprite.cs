﻿#region

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace ShEngine.EntitySystem.Graphics.Plane
{
    /// <summary>
    ///     Single sprite of a spriteset
    /// </summary>
    public class Sprite
    {
        #region Fields

        public readonly Rectangle SourceRect;
        public readonly Texture2D SpriteSet;

        #endregion

        #region Constructors

        public Sprite(Texture2D spriteSet, Rectangle? sourceRect)
        {
            SpriteSet = spriteSet;
            SourceRect = sourceRect.HasValue ? sourceRect.Value : spriteSet.Bounds;
        }

        #endregion

        #region Methods

        public void Draw(SpriteBatch batch, Rectangle rect, Color color, float zIndex = 0f, float rotation = 0f, float scale = 1f, SpriteEffects se = SpriteEffects.None)
        {
            Draw(batch, rect, color, new Vector2(), zIndex, rotation, scale, se);
        }

        public void Draw(SpriteBatch batch, Rectangle rect, Color color, Vector2 origin, float zIndex = 0f, float rotation = 0f, float scale = 1f,
            SpriteEffects se = SpriteEffects.None)
        {
            batch.Draw(SpriteSet, rect, SourceRect, color, rotation, origin, se, zIndex);
        }

        public void Draw(SpriteBatch batch, Vector2 position, Color color, float zIndex = 0f, float rotation = 0f, float scale = 1f, SpriteEffects se = SpriteEffects.None)
        {
            Draw(batch, position, color, new Vector2(), zIndex, rotation, scale, se);
        }

        public void Draw(SpriteBatch batch, Vector2 position, Color color, Vector2 origin, float zIndex = 0f, float rotation = 0f, float scale = 1f,
            SpriteEffects se = SpriteEffects.None)
        {
            batch.Draw(SpriteSet, position, SourceRect, color, rotation, origin, scale, se, zIndex);
        }

        #endregion
    }
}