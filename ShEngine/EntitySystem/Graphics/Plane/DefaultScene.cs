﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NetComponentEntitySystem;
using ShEngine.Common;
using ShEngine.EntitySystem.Components;

namespace ShEngine.EntitySystem.Graphics.Plane
{
    class DefaultScene : Scene
    {
        private EntityWorld _world;

        public DefaultScene(ShGame game, string name)
            : base(game, name)
        {
        }

        public override void Initialize()
        {
            _world = new EntityWorld();

            //_world.AddSystem(new RenderingSystem(), ExecutionMode.Synchronous, GameLoopType.DrawLoop);
            //_world.AddSystem(new MovementSystem(), ExecutionMode.ThreadedSync, GameLoopType.UpdateLoop);
            //_world.AddSystem(new FrameRateLog(), ExecutionMode.ThreadedFast, GameLoopType.UpdateLoop);
            //_world.AddSystem(new FrameRateLog(), ExecutionMode.ThreadedSync, GameLoopType.UpdateLoop);

            var texture = AssetManager.GetXna<Texture2D>("test");
            Random r = new Random();
            for (int i = 0; i < 5000; i++)
            {
                var e = new Entity(_world);
                

                e.AddComponent<Transformable2>();
                //e.AddScript<FpsCounter>();

                e.AddComponent(new Drawable(new Sprite(texture, null)));
                //e.AddComponent((IMovable)new Movable(new Vector2(r.Next(i), r.Next(i))));
                _world.AddEntity(e);
            }

            

            GameServices.GetService<ShGame>().Disposed += _world.Destroy;
        }

        public override void Draw(GameTime time)
        {
            _world.Draw(time.ElapsedGameTime.Milliseconds);
        }

        public override void Update(GameTime time)
        {
            _world.Update(time.ElapsedGameTime.Milliseconds);
        }
    }
}
