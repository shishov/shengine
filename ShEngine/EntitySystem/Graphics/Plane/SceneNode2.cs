﻿#region

using System.Collections.Generic;
using Microsoft.Xna.Framework;

#endregion

namespace ShEngine.EntitySystem.Graphics.Plane
{
    public class SceneNode2 : ITransformableNode<Transform2>
    {
        #region Fields

        private readonly SceneManager2 _manager;

        #endregion

        #region Constructors

        public SceneNode2(SceneManager2 manager, SceneNode2 parent, string name = "")
        {
            ChildNodes = new List<ITransformableNode<Transform2>>();
            Parent = parent;
            Name = name;

            _manager = manager;

            World = Parent == null ? Transform2.CreateZero() : Parent.World;
            Local = Transform2.CreateZero();
        }

        #endregion

        #region Properties

        public Vector2 PositionW
        {
            get { return World.Position; }
        }

        public Vector2 PositionL
        {
            get { return Local.Position; }
            set { ApplyLocalTransform(Transform2.CreateTranslation(value - Local.Position)); }
        }

        public float RotationW
        {
            get { return World.Rotation; }
        }

        public float RotationL
        {
            get { return Local.Rotation; }
            set { ApplyLocalTransform(Transform2.CreateRotation(value - Local.Rotation)); }
        }

        public Vector2 ScaleL
        {
            get { return Local.Scale; }
            set { ApplyLocalTransform(Transform2.CreateScale(value/Local.Scale)); }
        }

        #endregion

        #region ITransformableNode<Transform2> Members

        public string Name { get; private set; }
        public List<ITransformableNode<Transform2>> ChildNodes { get; private set; }
        public ITransformableNode<Transform2> Parent { get; private set; }
        public Transform2 World { get; private set; }
        public Transform2 Local { get; private set; }

        public ITransformableNode<Transform2> CreateChild(string name = "")
        {
            var node = _manager.CreateNode(this, name);
            ChildNodes.Add(node);
            return node;
        }

        public void ApplyLocalTransform(Transform2 delta)
        {
            Local = Local*delta;
            World = Local*Parent.World;

            foreach (var node in ChildNodes)
            {
                node.UpdateWorld();
            }
        }

        public void UpdateWorld()
        {
            World = Local*Parent.World;

            foreach (var node in ChildNodes)
            {
                node.UpdateWorld();
            }
        }

        #endregion

        #region Methods

        public void Move(Vector2 vec)
        {
            ApplyLocalTransform(Transform2.CreateTranslation(vec));
        }

        public void Rotate(float angle)
        {
            ApplyLocalTransform(Transform2.CreateRotation(angle));
        }

        public void Scale(float amount)
        {
            ApplyLocalTransform(Transform2.CreateScale(amount));
        }

        public void Scale(Vector2 amount)
        {
            ApplyLocalTransform(Transform2.CreateScale(amount));
        }

        #endregion
    }
}