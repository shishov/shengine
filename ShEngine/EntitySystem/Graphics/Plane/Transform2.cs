﻿#region

using System;
using Microsoft.Xna.Framework;
using ShEngine.Utitlities;

#endregion

namespace ShEngine.EntitySystem.Graphics.Plane
{
    /// <summary>
    ///     Struct that contains all information of transformation in 2D, similar to Matrix in 3D
    /// </summary>
    public struct Transform2
    {
        #region Fields

        public Vector2 Position;
        public float Rotation;
        public Vector2 Scale;

        #endregion

        #region Constructors

        public Transform2(Vector2 position, Vector2 scale, float rotation)
        {
            Position = position;
            Scale = scale;
            Rotation = rotation;
        }

        public Transform2(Vector2 position)
        {
            Position = position;
            Scale = new Vector2(1, 1);
            Rotation = 0;
        }

        #endregion

        #region Methods

        public static Transform2 CreateTranslation(Vector2 translation)
        {
            return new Transform2(translation);
        }

        public static Transform2 CreateRotation(float rotation)
        {
            return new Transform2(Vector2.Zero, new Vector2(1, 1), rotation);
        }

        public static Transform2 CreateScale(Vector2 scale)
        {
            return new Transform2(Vector2.Zero, scale, 0f);
        }

        public static Transform2 CreateScale(float scale)
        {
            return new Transform2(Vector2.Zero, new Vector2(scale, scale), 0f);
        }

        public static Transform2 CreateZero()
        {
            return new Transform2(Vector2.Zero, new Vector2(1, 1), 0);
        }

        public override string ToString()
        {
            return String.Format("Pos:{0} Rot:{1} Scale:{2}", Position, Rotation, Scale);
        }

        #endregion

        /// <summary>
        ///     Applyes transfromation b to transformation a
        /// </summary>
        public static Transform2 operator *(Transform2 a, Transform2 b)
        {
            Vector2 tmp1, tmp2;

            ExtensionMethods.Rotate(ref a.Position, b.Rotation, out tmp1);
            Vector2.Multiply(ref tmp1, ref b.Scale, out tmp2);
            Vector2.Add(ref b.Position, ref tmp2, out tmp1);
            Vector2.Multiply(ref a.Scale, ref b.Scale, out tmp2);

            return new Transform2(tmp1, tmp2, a.Rotation + b.Rotation);
        }
    }
}