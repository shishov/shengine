﻿#region

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace ShEngine.EntitySystem.Graphics.Plane
{
    /// <summary>
    ///     Simple camera implementation
    /// </summary>
    public class Camera2 : Transformable<SceneNode2, Transform2>
    {
        #region Fields

        private Matrix _transform;

        #endregion

        #region Methods

        public Matrix GetTransformation(GraphicsDevice graphicsDevice)
        {
            _transform = Matrix.CreateTranslation(new Vector3(-Node.PositionW.X, -Node.PositionW.Y, 0))*Matrix.CreateRotationZ(Node.RotationW)*
                         Matrix.CreateScale(new Vector3(Node.ScaleL.X, Node.ScaleL.Y, 0))*
                         Matrix.CreateTranslation(new Vector3(graphicsDevice.Viewport.Width*0.5f, graphicsDevice.Viewport.Height*0.5f, 0));
            return _transform;
        }

        #endregion
    }
}