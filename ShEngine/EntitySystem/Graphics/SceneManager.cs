﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace ShEngine.EntitySystem.Graphics
{
    /// <summary>
    ///     Abstract SceneManager handles some operations above nodes.
    /// </summary>
    /// <typeparam name="TNodyType">Type of the node</typeparam>
    /// <typeparam name="TTransform">Type of transform component (used for type check only)</typeparam>
    public abstract class SceneManager<TNodyType, TTransform> where TNodyType : ITransformableNode<TTransform>
    {
        #region Fields

        private readonly List<TNodyType> _nodes;

        #endregion

        #region Constructors

        protected SceneManager()
        {
            _nodes = new List<TNodyType>();
            Root = (TNodyType) Activator.CreateInstance(typeof (TNodyType), this, null, "Root");
        }

        #endregion

        #region Properties

        public TNodyType Root { get; private set; }

        #endregion

        #region Methods

        public TNodyType CreateNode(TNodyType parent, string name)
        {
            var node = (TNodyType) Activator.CreateInstance(typeof (TNodyType), this, parent, name);
            _nodes.Add(node);
            return node;
        }

        public void RemoveNodeByName(string name)
        {
            RemoveNode(GetNodeByName(name));
        }

        public void RemoveNode(TNodyType node)
        {
            foreach (var child in node.ChildNodes)
            {
                RemoveNode((TNodyType) child);
            }

            _nodes.Remove(node);
        }

        public TNodyType GetNodeByName(string name)
        {
            return _nodes.Find(n => Equals(name, n.Name));
        }

        #endregion
    }
}