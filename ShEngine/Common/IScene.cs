﻿#region

using Microsoft.Xna.Framework;

#endregion

namespace ShEngine.Common
{
    public interface IScene
    {
        string Name { get; }
        void Initialize();
        void Draw(GameTime gameTime);
        void Update(GameTime gameTime);
    }
}