﻿#region

using Microsoft.Xna.Framework;

#endregion

namespace ShEngine.Common
{
    public abstract class Scene : IScene
    {
        protected Scene(ShGame game, string name)
        {
            Name = name;
            Game = game;
        }

        #region IScene Members

        public string Name { get; private set; }
        public ShGame Game { get; private set; }

        public abstract void Initialize();
        public abstract void Draw(GameTime time);
        public abstract void Update(GameTime time);

        #endregion
    }
}