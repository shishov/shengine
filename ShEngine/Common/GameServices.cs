﻿#region

using Microsoft.Xna.Framework;

#endregion

namespace ShEngine.Common
{
    /// <summary>
    ///     Collection of gameservices
    /// </summary>
    public static class GameServices
    {
        #region Constants

        private static GameServiceContainer _container;

        #endregion

        #region Properties

        private static GameServiceContainer Instance
        {
            get { return _container ?? (_container = new GameServiceContainer()); }
        }

        #endregion

        #region Methods

        public static T GetService<T>()
        {
            return (T) Instance.GetService(typeof (T));
        }

        public static void AddService<T>(T service)
        {
            Instance.AddService(typeof (T), service);
        }

        public static void RemoveService<T>()
        {
            Instance.RemoveService(typeof (T));
        }

        #endregion
    }
}