﻿#region

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace ShEngine.Common
{
    /// <summary>
    ///     Default game class, use it instead of basic Game
    /// </summary>
    public abstract class ShGame : Game
    {
        protected readonly List<IScene> Scenes;
        protected readonly Object DrawLock;

        protected ShGame()
        {
            Scenes = new List<IScene>();
            Content.RootDirectory = "Content";
            var graphics = new GraphicsDeviceManager(this);
            DrawLock = new object();

            GameServices.AddService(graphics);
            GameServices.AddService(this);
            GameServices.AddService(Content);
            AssetManager.Set("DrawLock", DrawLock);
        }

        public IScene ActiveScene { get; private set; }

        protected override void Initialize()
        {
            GameServices.AddService(new SpriteBatch(GraphicsDevice));
            GameServices.AddService(GraphicsDevice);
            base.Initialize();
        }

        protected override void Draw(GameTime gameTime)
        {
            lock (DrawLock)
            {
                if (ActiveScene != null)
                    ActiveScene.Draw(gameTime);
                base.Draw(gameTime);
            }
        }

        protected override void Update(GameTime gameTime)
        {
            Input.Update(gameTime);
            if (ActiveScene != null)
                ActiveScene.Update(gameTime);
            base.Update(gameTime);
        }

        public void SetActiveScene(string name)
        {
            var scene = Scenes.Find(s => s.Name == name);
            if (scene == null)
                throw new Exception("Scene with this name doesn't exist");
            SetActiveScene(scene);
        }

        public void SetActiveScene(IScene scene)
        {
            ActiveScene = scene;
            scene.Initialize();
        }

        public void AddScene(IScene scene)
        {
            Scenes.Add(scene);
        }

        public void AddAndSetAvtice(IScene scene)
        {
            AddScene(scene);
            SetActiveScene(scene);
        }
    }
}