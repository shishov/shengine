﻿#region

using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

#endregion

namespace ShEngine.Common
{
    /// <summary>
    ///     Static managment of all assets
    /// </summary>
    public static class AssetManager
    {
        #region Constants

        private static readonly Dictionary<string, object> _objects = new Dictionary<string, object>();

        #endregion

        #region Methods

        /// <summary>
        ///     Lazy loads an xna asset
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T GetXna<T>(string path)
        {
            Object assetObject;
            if (_objects.TryGetValue(path, out assetObject))
                return (T) assetObject;

            var content = GameServices.GetService<ContentManager>();
            var obj = content.Load<T>(path);
            _objects.Add(path, obj);
            return obj;
        }

        /// <summary>
        ///     Gets resource
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T Get<T>(string path)
        {
            return (T) _objects[path];
        }

        /// <summary>
        ///     Sets resource
        /// </summary>
        /// <param name="path"></param>
        /// <param name="obj"></param>
        public static void Set(string path, object obj)
        {
            _objects.Add(path, obj);
        }

        #endregion
    }
}