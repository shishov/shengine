﻿#region

using System.Collections.Generic;
using Microsoft.Xna.Framework;

#endregion

namespace ShEngine.ScreenSystem
{
    /// <summary>
    ///     This class is a collection/stack of active screens in the game.
    ///     A game should have one screen manager and control the active screens by
    ///     adding and removing screens from this manager.
    ///     The top screen of the stack will be updated and the top screens that are
    ///     translucent will be drawn. The first screen on the stack that isn't translucent
    ///     will stop the drawing.
    /// </summary>
    /// By Koen Bollen, 2011
    public static class ScreenManager
    {
        #region Constants

        private static Stack<Screen> _screens;

        #endregion

        #region Properties

        public static bool Initialized { get; private set; }

        public static Screen ActiveScreen
        {
            get { return Peek(); }
        }

        #endregion

        #region Methods

        public static void Initialize(Screen start)
        {
            _screens = new Stack<Screen>();
            if (start != null)
                Push(start);
            Initialized = false;

            foreach (var s in _screens)
            {
                s.Initialize();
            }
            Initialized = true;
        }

        /// <summary>
        ///     Only update the top of the screen stack.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public static void Update(GameTime gameTime)
        {
            // TODO: Update translucent screens
            _screens.Peek().Update(gameTime);
        }

        /// <summary>
        ///     Draw all visible screens. A screen that is not translucent will stop the iteration of screens.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public static void Draw(GameTime gameTime)
        {
            // TODO: Draw translucent screens
            if (ActiveScreen != null)
                ActiveScreen.Draw(gameTime);
        }

        /// <summary>
        ///     Add a screen to the top of the stack, if the manager is initialized but the screen isn't initialize it.
        ///     Also set it's manager to this.
        /// </summary>
        /// <param name="screen">The screen to add.</param>
        public static void Push(Screen screen)
        {
            if (!screen.Initialized && Initialized)
                screen.Initialize();
            if (ActiveScreen != null)
                ActiveScreen.Deactivated();
            _screens.Push(screen);
        }

        /// <summary>
        ///     Get the top of the screen stack. The most active screen.
        /// </summary>
        /// <returns>The active screen.</returns>
        public static Screen Peek()
        {
            return _screens.Count < 1 ? null : _screens.Peek();
        }

        /// <summary>
        ///     Remove a screen from the screen stack.
        /// </summary>
        /// <returns>The removed screen.</returns>
        public static Screen Pop()
        {
            if (_screens.Count < 1)
                return null;
            var prev = _screens.Pop();
            if (ActiveScreen != null)
                ActiveScreen.Activated();
            return prev;
        }

        #endregion
    }
}