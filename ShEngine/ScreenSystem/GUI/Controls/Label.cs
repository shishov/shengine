﻿#region

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShEngine.Common;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    /// <summary>
    ///     Text output control
    /// </summary>
    public class Label : IControl
    {
        #region Fields

        private readonly SpriteFont _font;
        public Color HoverTextColor;

        public Action OnClickAction;
        public Action OnFocusAction;
        public Action OnHoverAction;
        public Action OnLostFocusAction;
        public String Text;
        public Color TextColor;
        private Rectangle _bounds;

        private bool _hovering;
        private Vector2 _position;

        #endregion

        #region Constructors

        public Label(string text, string fontName = "main")
        {
            Text = text;
            _font = AssetManager.GetXna<SpriteFont>(fontName);
            var textSize = _font.MeasureString(text);
            _bounds = new Rectangle {Width = (int) textSize.X, Height = (int) textSize.Y};

            TextColor = DefaultColorScheme.UITextColor;
            HoverTextColor = DefaultColorScheme.HighlighedTextColor;
        }

        #endregion

        #region Properties

        public int Width
        {
            get { return _bounds.Width; }
            set { _bounds.Width = value; }
        }

        public int Height
        {
            get { return _bounds.Height; }
            set { _bounds.Height = value; }
        }

        #endregion

        #region IControl Members

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                _bounds.X = (int) value.X;
                _bounds.Y = (int) value.Y;
            }
        }

        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        public void Draw(SpriteBatch sBatch)
        {
            if (!_hovering)
                sBatch.DrawString(_font, Text, _position, TextColor);
            else
                sBatch.DrawString(_font, Text, _position, HoverTextColor);
        }

        public void Update(GameTime dateTime)
        {
            _hovering = false;
        }

        public void OnClick(Point cursor)
        {
            if (OnClickAction != null)
                OnClickAction();
        }

        public void OnHover(Point cursor)
        {
            _hovering = true;
            if (OnFocusAction != null)
                OnFocusAction();
        }

        public void OnFocus()
        {
            if (OnHoverAction != null)
                OnHoverAction();
        }

        public void OnLostFocus()
        {
            if (OnLostFocusAction != null)
                OnLostFocusAction();
        }

        #endregion
    }
}