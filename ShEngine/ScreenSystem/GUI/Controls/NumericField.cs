﻿#region

using System;
using System.Globalization;
using Microsoft.Xna.Framework;
using ShEngine.Common;
using ShEngine.EntitySystem.Graphics.Plane;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    public class NumericField : HorizontalGroup
    {
        #region Fields

        private readonly Label _valLbl;
        public float Delta;
        public Action OnMinus;
        public Action OnPlus;

        private float _value;

        #endregion

        #region Constructors

        public NumericField()
        {
            Spacing = 10;
            Delta = 1;

            _valLbl = new Label("0", "tiny");

            var plus = new Button
            {
                Icon = AssetManager.GetXna<Sprite>("plus"),
                IconMargin = new Vector2(2, 2),
                DrawIcon = true,
                IconWidth = 16,
                IconHeight = 16,
                Width = 20,
                Height = 20
            };
            plus.OnClickAction += OnPlus;
            plus.OnClickAction += () => Value += Delta;

            var minus = new Button
            {
                Icon = AssetManager.GetXna<Sprite>("minus"),
                DrawIcon = true,
                IconMargin = new Vector2(2, 2),
                IconWidth = 16,
                IconHeight = 16,
                Width = 20,
                Height = 20
            };
            minus.OnClickAction += OnMinus;
            minus.OnClickAction += () => Value -= Delta;

            Add(minus);
            Add(_valLbl);
            Add(plus);
        }

        #endregion

        #region Properties

        public float Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _valLbl.Text = _value.ToString(CultureInfo.InvariantCulture);
            }
        }

        #endregion
    }
}