﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    public class VerticalGroup : IControl
    {
        #region Fields

        /// <summary>
        ///     Child controls
        /// </summary>
        public List<IControl> Childs;

        /// <summary>
        ///     Spacing between elements, default is 1px
        /// </summary>
        public int Spacing = 1;

        private Rectangle _bounds;

        #endregion

        #region Constructors

        public VerticalGroup()
        {
            Childs = new List<IControl>();
            _bounds = new Rectangle();
        }

        #endregion

        #region IControl Members

        public virtual Vector2 Position
        {
            get { return new Vector2(_bounds.X, _bounds.Y); }
            set
            {
                var diff = value - new Vector2(_bounds.X, _bounds.Y);
                _bounds.X = (int) value.X;
                _bounds.Y = (int) value.Y;
                foreach (var b in Childs)
                {
                    b.Position += diff;
                }
            }
        }

        public virtual Rectangle Bounds
        {
            get { return _bounds; }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (var b in Childs)
            {
                b.Draw(spriteBatch);
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (var b in Childs)
            {
                b.Update(gameTime);
            }
        }

        public virtual void OnClick(Point cursor)
        {
            foreach (var control in Childs)
            {
                if (control.Bounds.Contains(cursor))
                {
                    control.OnClick(cursor);
                    break;
                }
            }
        }

        public virtual void OnHover(Point cursor)
        {
            foreach (var control in Childs)
            {
                if (control.Bounds.Contains(cursor))
                {
                    control.OnHover(cursor);
                    break;
                }
            }
        }

        public virtual void OnFocus()
        {
        }

        public virtual void OnLostFocus()
        {
        }

        #endregion

        #region Methods

        public virtual void Add(IControl control)
        {
            if (Childs.Count == 0)
            {
                control.Position = Position;
                _bounds = control.Bounds;
            }
            else
            {
                // Set position of control to bottom border of
                // last element in group
                control.Position = Childs.Last().Position + new Vector2(0, Childs.Last().Bounds.Height + Spacing);

                // Extend bounds
                _bounds.Height += control.Bounds.Height + Spacing;
                _bounds.Width = Math.Max(_bounds.Width, control.Bounds.Width);
            }

            Childs.Add(control);
        }

        #endregion
    }
}