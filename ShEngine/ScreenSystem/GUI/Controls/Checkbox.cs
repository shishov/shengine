﻿#region

using Microsoft.Xna.Framework;
using ShEngine.Common;
using ShEngine.EntitySystem.Graphics.Plane;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    public class Checkbox : Image
    {
        #region Preperties

        public bool Value
        {
            get { return _value; }
            set
            {
                _value = value;
                Sprite = _value ? AssetManager.GetXna<Sprite>("checked") : AssetManager.GetXna<Sprite>("unchecked");
            }
        }

        #endregion

        #region Fields

        private bool _value;

        #endregion

        #region Constructors

        public Checkbox(bool defaultValue = false) : base("unchecked")
        {
            Width = 18;
            Height = 18;
            ImageColor = DefaultColorScheme.UITextColor;
            HoverImageColor = DefaultColorScheme.HighlighedTextColor;
            Value = defaultValue;
        }

        #endregion

        #region Methods

        public override void OnClick(Point cursor)
        {
            Value = !_value;
            base.OnClick(cursor);
        }

        #endregion
    }
}