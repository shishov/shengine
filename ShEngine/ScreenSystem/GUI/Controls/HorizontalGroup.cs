﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    public class HorizontalGroup : IControl
    {
        #region Fields

        /// <summary>
        ///     Child controls
        /// </summary>
        public List<IControl> Childs;

        public int ElementWidth = 20;
        public bool FixedElementWidth = false;

        /// <summary>
        ///     Spacing between elements
        /// </summary>
        public int Spacing;

        private Rectangle _bounds;

        #endregion

        #region Constructors

        public HorizontalGroup()
        {
            Childs = new List<IControl>();
            _bounds = new Rectangle();
            Position = Vector2.Zero;
        }

        #endregion

        #region IControl Members

        public Vector2 Position
        {
            get { return new Vector2(_bounds.X, _bounds.Y); }
            set
            {
                var diff = value - new Vector2(_bounds.X, _bounds.Y);
                _bounds.X = (int) value.X;
                _bounds.Y = (int) value.Y;
                foreach (var b in Childs)
                {
                    b.Position += diff;
                }
            }
        }

        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var b in Childs)
            {
                b.Draw(spriteBatch);
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (var b in Childs)
            {
                b.Update(gameTime);
            }
        }

        public void OnClick(Point cursor)
        {
            foreach (var control in Childs)
            {
                if (control.Bounds.Contains(cursor))
                {
                    control.OnClick(cursor);
                    break;
                }
            }
        }

        public void OnHover(Point cursor)
        {
            foreach (var control in Childs)
            {
                if (control.Bounds.Contains(cursor))
                {
                    control.OnHover(cursor);
                    break;
                }
            }
        }

        public void OnFocus()
        {
        }

        public void OnLostFocus()
        {
        }

        #endregion

        #region Methods

        public void Add(IControl control)
        {
            if (Childs.Count == 0)
            {
                control.Position = Position;
                _bounds = control.Bounds;
            }
            else
            {
                // Set position of control to right border of
                // last element in group
                control.Position = Childs.Last().Position + new Vector2(Childs.Last().Bounds.Width + Spacing, 0);

                // Extend bounds
                _bounds.Width += control.Bounds.Width + Spacing;
                _bounds.Height = Math.Max(_bounds.Height, control.Bounds.Height);
            }

            Childs.Add(control);
        }

        #endregion
    }
}