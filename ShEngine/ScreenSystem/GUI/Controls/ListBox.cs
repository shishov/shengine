﻿#region

using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ShEngine.Common;
using ShEngine.EntitySystem.Graphics.Plane;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    public class ListBox : IControl
    {
        #region Fields

        private readonly GraphicsDevice _graphicsDevice;
        private readonly Sprite _slider;
        private readonly float _sliderSpeed;
        public Color BackgroundColor;
        public VerticalGroup Group;
        public Color SliderColor;
        private Rectangle _bounds;
        private bool _hovering;
        private int _lastScrollWheelValue;
        private Vector2 _offset;
        private Texture2D _resultTexture;
        private Rectangle _sliderBounds;
        private float _sliderK;

        #endregion

        #region Constructors

        public ListBox()
        {
            Group = new VerticalGroup();
            _bounds = new Rectangle();

            BackgroundColor = DefaultColorScheme.UIBgrColor2;
            SliderColor = DefaultColorScheme.UITextColor;

            Height = 300;
            Width = 200;
            _sliderSpeed = 30;
            _sliderK = 0;
            _hovering = false;

            _offset = Vector2.Zero;
            _sliderBounds = new Rectangle(_bounds.Right - 10, _bounds.Top, 10, 30);
            _slider = AssetManager.GetXna<Sprite>("rect");

            _graphicsDevice = GameServices.GetService<GraphicsDevice>();
            RenderView();
        }

        #endregion

        #region Properties

        public int Width
        {
            get { return _bounds.Width; }
            set
            {
                _bounds.Width = value;
                _sliderBounds = new Rectangle(_bounds.Right - 10, _bounds.Top, 10, 30);
            }
        }

        public int Height
        {
            get { return _bounds.Height; }
            set
            {
                _bounds.Height = value;
                _sliderBounds = new Rectangle(_bounds.Right - 10, _bounds.Top, 10, 30);
            }
        }

        #endregion

        #region IControl Members

        public Vector2 Position
        {
            get { return new Vector2(_bounds.X, _bounds.Y); }
            set
            {
                _bounds.X = (int) value.X;
                _bounds.Y = (int) value.Y;
                _sliderBounds = new Rectangle(_bounds.Right - 10, _bounds.Top, 10, 30);
            }
        }

        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (_resultTexture != null)
                spriteBatch.Draw(_resultTexture, _bounds, Color.White);

            _slider.Draw(spriteBatch, _sliderBounds, SliderColor);
        }

        public void Update(GameTime gameTime)
        {
            if (_hovering)
            {
                var delta = Input.MouseState.ScrollWheelValue - _lastScrollWheelValue;
                if (delta != 0)
                {
                    MoveSlider(-(int) (delta*_sliderSpeed/240));
                    _lastScrollWheelValue += delta;
                }
            }

            _hovering = false;

            Group.Update(gameTime);
        }

        public void OnClick(Point cursor)
        {
            cursor.X -= (int) Position.X;
            cursor.Y -= (int) Position.Y;
            Group.OnClick(cursor);
        }

        public void OnHover(Point cursor)
        {
            _lastScrollWheelValue = Input.MouseState.ScrollWheelValue;
            _hovering = true;

            if (_sliderBounds.Contains(cursor) && Input.MouseState.LeftButton == ButtonState.Pressed && Input.IsCursorMoved)
            {
                var y = Input.MouseState.Y - Input.PreviousMouseState.Y;
                MoveSlider(y);
            }

            cursor.X -= (int) Position.X;
            cursor.Y -= (int) Position.Y;
            Group.OnHover(cursor);
            RenderView();
        }

        public void OnFocus()
        {
        }

        public void OnLostFocus()
        {
        }

        #endregion

        #region Methods

        public void Add(IControl control)
        {
            Group.Add(control);
            RenderView();

            _sliderK = (Group.Childs.Last().Position.Y + Group.Childs.Last().Bounds.Height)/Height;
        }

        private void MoveSlider(int amount)
        {
            var lastSliderY = _sliderBounds.Y;
            _sliderBounds.Y += amount;
            _sliderBounds.Y = Math.Min(_sliderBounds.Y, _bounds.Bottom - _sliderBounds.Height);
            _sliderBounds.Y = Math.Max(_sliderBounds.Y, _bounds.Top);

            var dy = (int) ((_sliderBounds.Y - lastSliderY)*_sliderK);
            Group.Position -= new Vector2(0, dy);
        }

        private void RenderView()
        {
            var target = new RenderTarget2D(_graphicsDevice, _bounds.Width, _bounds.Height);
            _graphicsDevice.SetRenderTarget(target);

            _graphicsDevice.Clear(BackgroundColor);
            var batch = new SpriteBatch(_graphicsDevice);

            batch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            Group.Draw(batch);
            batch.End();

            _resultTexture = target;
            _graphicsDevice.SetRenderTarget(null);
        }

        #endregion
    }
}