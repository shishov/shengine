﻿#region

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShEngine.Common;
using ShEngine.EntitySystem.Graphics.Plane;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    public class Button : IControl
    {
        #region Fields

        public Color BackgroundColor;
        public Sprite Bgr;
        public Boolean DrawBackground;
        public Boolean DrawIcon;
        public SpriteFont Font;
        public Color HoverBackgroundColor;
        public Color HoverIconColor;
        public Color HoverTextColor;
        public Color IconColor;
        public Int32 IconHeight;
        public Vector2 IconMargin;
        public Int32 IconWidth;
        public Action OnClickAction;
        public Action OnFocusAction;
        public Action OnHoverAction;
        public Action OnLostFocusAction;
        public String Text;
        public Color TextColor;
        public Vector2 TextMargin;
        private Rectangle _bounds;
        private Sprite _icon;
        private Boolean _isHover;

        #endregion

        #region Constructors

        public Button()
        {
            // load resources            
            Bgr = AssetManager.GetXna<Sprite>("rect");
            Font = AssetManager.GetXna<SpriteFont>("main");

            // Default style
            IconColor = Color.White;
            HoverIconColor = Color.White;

            TextColor = DefaultColorScheme.UITextColor;
            HoverTextColor = DefaultColorScheme.HighlighedTextColor;

            BackgroundColor = DefaultColorScheme.UIBgrColor;
            HoverBackgroundColor = DefaultColorScheme.HighlighedBgrColor;

            IconMargin = new Vector2(15, 10);
            TextMargin = new Vector2(10, 5);
            DrawBackground = true;

            // perform basic placement
            Position = Vector2.Zero;
        }

        #endregion

        #region Properties

        public int Width
        {
            get { return _bounds.Width; }
            set { _bounds.Width = value; }
        }

        public int Height
        {
            get { return _bounds.Height; }
            set { _bounds.Height = value; }
        }

        public Sprite Icon
        {
            get { return _icon; }
            set
            {
                _icon = value;
                IconWidth = _icon.SourceRect.Width;
                IconHeight = _icon.SourceRect.Height;
            }
        }

        #endregion

        #region IControl Members

        public Vector2 Position
        {
            get { return new Vector2(_bounds.X, _bounds.Y); }
            set
            {
                _bounds.X = (int) value.X;
                _bounds.Y = (int) value.Y;
            }
        }

        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        public void Draw(SpriteBatch sBatch)
        {
            var bgColor = BackgroundColor;
            var txColor = TextColor;
            var icColor = IconColor;

            if (_isHover)
            {
                bgColor = HoverBackgroundColor;
                txColor = HoverTextColor;
                icColor = HoverIconColor;
            }

            if (DrawBackground)
                Bgr.Draw(sBatch, _bounds, bgColor);

            if (DrawIcon)
            {
                var iconRect = new Rectangle(_bounds.X + (int) IconMargin.X, _bounds.Y + (int) IconMargin.Y, IconWidth, IconHeight);
                Icon.Draw(sBatch, iconRect, icColor);

                if (!string.IsNullOrEmpty(Text))
                    sBatch.DrawString(Font, Text, new Vector2(iconRect.Right + TextMargin.X, _bounds.Y + TextMargin.Y), txColor);
            }
            else
            {
                if (!string.IsNullOrEmpty(Text))
                    sBatch.DrawString(Font, Text, new Vector2(_bounds.X + TextMargin.X, _bounds.Y + TextMargin.Y), txColor);
            }
        }

        public void Update(GameTime dateTime)
        {
            _isHover = false;
        }

        public void OnClick(Point cursor)
        {
            if (OnClickAction != null)
                OnClickAction();
        }

        public void OnHover(Point cursor)
        {
            _isHover = true;
            if (OnFocusAction != null)
                OnFocusAction();
        }

        public void OnFocus()
        {
            if (OnHoverAction != null)
                OnHoverAction();
        }

        public void OnLostFocus()
        {
            if (OnLostFocusAction != null)
                OnLostFocusAction();
        }

        #endregion

        #region Methods

        public void SetIcon(string iconKey)
        {
            Icon = AssetManager.GetXna<Sprite>(iconKey);
            IconWidth = Icon.SourceRect.Width;
            IconHeight = Icon.SourceRect.Height;
        }

        public void SetOptimalBounds()
        {
            // only Icon
            if (string.IsNullOrEmpty(Text))
            {
                _bounds.Width = (int) (IconMargin.X*2 + IconWidth + TextMargin.X*2);
                _bounds.Height = (int) (IconMargin.Y*2 + IconHeight);
            }
            else
            {
                var textSize = Font.MeasureString(Text);
                if (DrawIcon) // Text and icon                    
                {
                    _bounds.Width = (int) (IconMargin.X*2 + IconWidth + TextMargin.X*2 + textSize.X);
                    _bounds.Height = (int) Math.Max(IconMargin.Y*2 + IconHeight, TextMargin.Y*2 + textSize.Y);
                }
                else // Only Text
                {
                    _bounds.Width = (int) (TextMargin.X*2 + textSize.X);
                    _bounds.Height = (int) (TextMargin.Y*2 + textSize.Y);
                }
            }
        }

        #endregion
    }
}