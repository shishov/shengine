﻿#region

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShEngine.Common;
using ShEngine.EntitySystem.Graphics.Plane;

#endregion

namespace ShEngine.ScreenSystem.GUI.Controls
{
    /// <summary>
    ///     Image control, also can be a button control
    /// </summary>
    public class Image : IControl
    {
        #region Fields

        public Color HoverImageColor;
        public Color ImageColor;
        public Action OnClickAction;
        public Action OnFocusAction;
        public Action OnHoverAction;
        public Action OnLostFocusAction;
        public Sprite Sprite;
        private Rectangle _bounds;
        private bool _isHovering;

        #endregion

        #region Constructors

        public Image(string spriteKey)
        {
            Sprite = AssetManager.GetXna<Sprite>(spriteKey);
            _bounds = new Rectangle {Width = Sprite.SourceRect.Width, Height = Sprite.SourceRect.Height};
            ImageColor = Color.White;
            HoverImageColor = Color.White;
        }

        #endregion

        #region Properties

        public int Width
        {
            get { return _bounds.Width; }
            set { _bounds.Width = value; }
        }

        public int Height
        {
            get { return _bounds.Height; }
            set { _bounds.Height = value; }
        }

        #endregion

        #region IControl Members

        public Vector2 Position
        {
            get { return new Vector2(_bounds.X, _bounds.Y); }
            set
            {
                _bounds.X = (int) value.X;
                _bounds.Y = (int) value.Y;
            }
        }

        public Rectangle Bounds
        {
            get { return _bounds; }
        }

        public void Draw(SpriteBatch sBatch)
        {
            if (_isHovering)
                Sprite.Draw(sBatch, _bounds, HoverImageColor);
            else
                Sprite.Draw(sBatch, _bounds, ImageColor);
        }

        public void Update(GameTime dateTime)
        {
            _isHovering = false;
        }

        public virtual void OnClick(Point cursor)
        {
            if (OnClickAction != null)
                OnClickAction();
        }

        public void OnHover(Point cursor)
        {
            _isHovering = true;
            if (OnFocusAction != null)
                OnFocusAction();
        }

        public void OnFocus()
        {
            if (OnHoverAction != null)
                OnHoverAction();
        }

        public void OnLostFocus()
        {
            if (OnLostFocusAction != null)
                OnLostFocusAction();
        }

        #endregion
    }
}