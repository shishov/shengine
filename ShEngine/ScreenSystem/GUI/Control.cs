﻿#region

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

#endregion

namespace ShEngine.ScreenSystem.GUI
{
    /// <summary>
    ///     Base GUI control interface
    /// </summary>
    public interface IControl
    {
        #region Properties

        Vector2 Position { get; set; }
        Rectangle Bounds { get; }

        #endregion

        #region Methods

        void Draw(SpriteBatch spriteBatch);
        void Update(GameTime gameTime);
        void OnClick(Point cursor);
        void OnHover(Point cursor);
        void OnFocus();
        void OnLostFocus();

        #endregion
    }
}