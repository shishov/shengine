﻿#region

using System.Collections.Generic;
using Microsoft.Xna.Framework;

#endregion

namespace ShEngine.ScreenSystem.GUI
{
    public abstract class GuiScreen : Screen
    {
        #region Fields

        protected List<IControl> Controls;

        #endregion

        #region Constructors

        protected GuiScreen(Game game) : base(game, true)
        {
            Controls = new List<IControl>();
        }

        #endregion

        #region Properties

        public IControl Focused { get; protected set; }

        #endregion

        #region Methods

        public override void Update(GameTime gameTime)
        {
            foreach (var control in Controls)
            {
                control.Update(gameTime);

                if (!control.Bounds.Contains(new Point(Input.Cursor.X, Input.Cursor.Y)))
                    continue;

                control.OnHover(Input.Cursor);

                if (!Input.IsNewMouseButtonPress(MouseButtons.LeftButton))
                    continue;
                control.OnClick(Input.Cursor);

                if (Focused != null)
                    Focused.OnLostFocus();

                Focused = control;
                Focused.OnFocus();
                break;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            SBatch.Begin();
            foreach (var c in Controls)
            {
                c.Draw(SBatch);
            }
            SBatch.End();

            base.Draw(gameTime);
        }

        public virtual void Activate()
        {
            // TODO: implement IControl.Activate()
        }

        #endregion
    }
}