﻿#region

using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ShEngine.Common;

#endregion

namespace ShEngine.ScreenSystem
{
    public class Screen : DrawableGameComponent
    {
        #region Fields

        public SpriteBatch SBatch;

        #endregion

        #region Constructors

        public Screen(Game game, bool translucent = false) : base(game)
        {
            Components = new GameComponentCollection();
            Translucent = translucent;
            SBatch = new SpriteBatch(GameServices.GetService<GraphicsDevice>());
        }

        #endregion

        #region Properties

        public bool Initialized { get; private set; }
        public bool Translucent { get; set; }
        public GameComponentCollection Components { get; private set; }

        #endregion

        #region Methods

        public virtual void Activated()
        {
        }

        public virtual void Deactivated()
        {
        }

        public override void Initialize()
        {
            foreach (var gc in Components.Cast<GameComponent>())
            {
                gc.Initialize();
            }
            Initialized = true;
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            // Major credits to Nils Dijk:
            foreach (var gc in Components.OfType<IUpdateable>().Where<IUpdateable>(x => x.Enabled).OrderBy<IUpdateable, int>(x => x.UpdateOrder))
            {
                gc.Update(gameTime);
            }
            base.Update(gameTime);
        }


        public override void Draw(GameTime gameTime)
        {
            // Major credits to Nils Dijk:
            foreach (var gc in Components.OfType<IDrawable>().Where<IDrawable>(x => x.Visible).OrderBy<IDrawable, int>(x => x.DrawOrder))
            {
                gc.Draw(gameTime);
            }
            base.Draw(gameTime);
        }

        #endregion

        // Usable
    }
}