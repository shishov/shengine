﻿#region

using System;
using Microsoft.Xna.Framework;

#endregion

namespace ShEngine.Utitlities
{
    public class Transition<TFunction> where TFunction : IInterpolationFunc, new()
    {
        #region Delegates

        public delegate void EndTransitionHandler(object sender);

        #endregion

        #region Fields

        public readonly float End;
        public readonly float Start;

        private readonly TFunction _function;
        private readonly float _scaleK;
        private readonly float _targetTime;
        private float _currentTime;
        private bool _enabled;

        #endregion

        #region Constructors

        public Transition(float start, float end, float time)
        {
            _function = new TFunction();
            Start = start;
            End = end;
            _targetTime = time;
            _scaleK = (end - Start);
            _currentTime = 0;
            _enabled = true;
        }

        #endregion

        #region Properties

        public float Value
        {
            get { return Start + _function.Function(_currentTime/_targetTime)*_scaleK; }
        }

        public bool InProgress
        {
            get { return _enabled; }
        }

        #endregion

        #region Methods

        public void Step(float dt)
        {
            if (_currentTime < _targetTime)
                _currentTime += dt;
            else if (_enabled)
            {
                if (OnEndTransition != null)
                    OnEndTransition(this);
                _enabled = false;
            }
        }

        public void Step(GameTime gameTime)
        {
            Step(gameTime.ElapsedGameTime.Milliseconds);
        }

        #endregion

        public event EndTransitionHandler OnEndTransition;
    }

    /// <summary>
    ///     Interface for all easing functions
    ///     Function Value is from 0 to 1, while x is from 0 to 1 too
    /// </summary>
    public interface IInterpolationFunc
    {
        #region Methods

        float Function(float x);

        #endregion
    }

    public class Linear : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return x;
        }

        #endregion
    }

    public class InSquare : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return x*x;
        }

        #endregion
    }

    public class InCubic : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return x*x*x;
        }

        #endregion
    }

    public class InQuint : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return x*x*x*x;
        }

        #endregion
    }

    public class OutSquare : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return -(x - 1)*(x - 1) + 1;
        }

        #endregion
    }

    public class OutCubic : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return (x - 1)*(x - 1)*(x - 1) + 1;
        }

        #endregion
    }

    public class OutQuint : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return -(x - 1)*(x - 1)*(x - 1)*(x - 1) + 1;
        }

        #endregion
    }

    public class InElastic : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return (float) Math.Pow(MathHelper.E, 10*(x - 1))*(float) Math.Cos(x*6*MathHelper.Pi);
        }

        #endregion
    }

    public class OutElastic : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            return 1 - (float) Math.Pow(MathHelper.E, -10*x)*(float) Math.Cos(x*6*MathHelper.Pi);
        }

        #endregion
    }

    public class InOutSquare : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            if (x < 0.5f)
                return 2*x*x;

            x -= 1;
            return -2f*(x*x - 2) - 3;
        }

        #endregion
    }

    public class InOutCubic : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            if (x < 0.5f)
                return 4*x*x*x;

            x -= 1;
            return 4f*(x*x*x - 2) + 9;
        }

        #endregion
    }

    public class InOutQuint : IInterpolationFunc
    {
        #region IInterpolationFunc Members

        public float Function(float x)
        {
            if (x < 0.5f)
                return 8*x*x*x*x;

            x -= 1;
            return - 8f*(x*x*x*x - 2) - 15;
        }

        #endregion
    }
}