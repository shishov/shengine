﻿#region

using System.Collections.Generic;

#endregion

namespace ShEngine.Utitlities
{
    /// <summary>
    ///     Fast input translation service
    /// </summary>
    public static class InputTranslation
    {
        #region Constants

        private static readonly Dictionary<string, string> TranslationDict = new Dictionary<string, string>();

        #endregion

        #region Methods

        public static void Initialize()
        {
            TranslationDict.Add("S_A", "A");
            TranslationDict.Add("S_B", "B");
            TranslationDict.Add("S_C", "C");
            TranslationDict.Add("S_D", "D");
            TranslationDict.Add("S_E", "E");
            TranslationDict.Add("S_F", "F");
            TranslationDict.Add("S_G", "G");
            TranslationDict.Add("S_H", "H");
            TranslationDict.Add("S_I", "I");
            TranslationDict.Add("S_J", "J");
            TranslationDict.Add("S_K", "K");
            TranslationDict.Add("S_L", "L");
            TranslationDict.Add("S_M", "M");
            TranslationDict.Add("S_N", "N");
            TranslationDict.Add("S_O", "O");
            TranslationDict.Add("S_P", "P");
            TranslationDict.Add("S_Q", "Q");
            TranslationDict.Add("S_R", "R");
            TranslationDict.Add("S_S", "S");
            TranslationDict.Add("S_T", "T");
            TranslationDict.Add("S_U", "U");
            TranslationDict.Add("S_V", "V");
            TranslationDict.Add("S_W", "W");
            TranslationDict.Add("S_X", "X");
            TranslationDict.Add("S_Y", "Y");
            TranslationDict.Add("S_Z", "Z");
            TranslationDict.Add("A", "a");
            TranslationDict.Add("B", "b");
            TranslationDict.Add("C", "c");
            TranslationDict.Add("D", "d");
            TranslationDict.Add("E", "e");
            TranslationDict.Add("F", "f");
            TranslationDict.Add("G", "g");
            TranslationDict.Add("H", "h");
            TranslationDict.Add("I", "i");
            TranslationDict.Add("J", "j");
            TranslationDict.Add("K", "k");
            TranslationDict.Add("L", "l");
            TranslationDict.Add("M", "m");
            TranslationDict.Add("N", "n");
            TranslationDict.Add("O", "o");
            TranslationDict.Add("P", "p");
            TranslationDict.Add("Q", "q");
            TranslationDict.Add("R", "r");
            TranslationDict.Add("S", "s");
            TranslationDict.Add("T", "t");
            TranslationDict.Add("U", "u");
            TranslationDict.Add("V", "v");
            TranslationDict.Add("W", "w");
            TranslationDict.Add("X", "x");
            TranslationDict.Add("Y", "y");
            TranslationDict.Add("Z", "z");
            TranslationDict.Add("D0", "0");
            TranslationDict.Add("D1", "1");
            TranslationDict.Add("D2", "2");
            TranslationDict.Add("D3", "3");
            TranslationDict.Add("D4", "4");
            TranslationDict.Add("D5", "5");
            TranslationDict.Add("D6", "6");
            TranslationDict.Add("D7", "7");
            TranslationDict.Add("D8", "8");
            TranslationDict.Add("D9", "9");
            TranslationDict.Add("S_D0", ")");
            TranslationDict.Add("S_D1", "!");
            TranslationDict.Add("S_D2", "@");
            TranslationDict.Add("S_D3", "#");
            TranslationDict.Add("S_D4", "$");
            TranslationDict.Add("S_D5", "%");
            TranslationDict.Add("S_D6", "^");
            TranslationDict.Add("S_D7", "&");
            TranslationDict.Add("S_D8", "*");
            TranslationDict.Add("S_D9", "(");
            TranslationDict.Add("NumPad0", "0");
            TranslationDict.Add("NumPad1", "1");
            TranslationDict.Add("NumPad2", "2");
            TranslationDict.Add("NumPad3", "3");
            TranslationDict.Add("NumPad4", "4");
            TranslationDict.Add("NumPad5", "5");
            TranslationDict.Add("NumPad6", "6");
            TranslationDict.Add("NumPad7", "7");
            TranslationDict.Add("NumPad8", "8");
            TranslationDict.Add("NumPad9", "9");


            TranslationDict.Add("OemTilde", "`");
            TranslationDict.Add("S_OemTilde", "~");
            TranslationDict.Add("OemPipe", "\\");
            TranslationDict.Add("S_OemPipe", "|");
            TranslationDict.Add("OemPlus", "=");
            TranslationDict.Add("S_OemPlus", "+");
            TranslationDict.Add("OemMinus", "-");
            TranslationDict.Add("S_OemMinus", "_");


            TranslationDict.Add("OemCloseBrackets", "]");
            TranslationDict.Add("S_OemCloseBrackets", "}");
            TranslationDict.Add("OemOpenBrackets", "[");
            TranslationDict.Add("S_OemOpenBrackets", "{");
            TranslationDict.Add("OemQuestion", "/");
            TranslationDict.Add("S_OemQuestion", "?");
            TranslationDict.Add("OemQuotes", "'");
            TranslationDict.Add("S_OemQuotes", "\"");

            TranslationDict.Add("OemSemicolon", ";");
            TranslationDict.Add("S_OemSemicolon", ":");
            TranslationDict.Add("OemPeriod", ".");
            TranslationDict.Add("S_OemPeriod", ">");
            TranslationDict.Add("OemComma", ",");
            TranslationDict.Add("S_OemComma", "<");
            TranslationDict.Add("Decimal", ".");

            TranslationDict.Add("Divide", "/");
            TranslationDict.Add("Multiply", "*");
            TranslationDict.Add("Subtract", "-");
            TranslationDict.Add("Add", "+");

            TranslationDict.Add("Space", " ");
        }


        public static string Get(string keyCode, bool isShift)
        {
            if (isShift)
                keyCode = "S_" + keyCode;

            return TranslationDict.ContainsKey(keyCode) ? TranslationDict[keyCode] : "";
        }

        #endregion
    }
}