﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

#endregion

namespace ShEngine
{
    /// <summary>
    ///     an enum of all available mouse buttons.
    /// </summary>
    public enum MouseButtons
    {
        LeftButton,
        MiddleButton,
        RightButton,
        ExtraButton1,
        ExtraButton2
    }

    /// <summary>
    ///     MAges al the inputs (mouse, keyboard, gamepad)
    /// </summary>
    public static class Input
    {
        private static GamePadState _currentGamePadState;
        private static KeyboardState _currentKeyboardState;
        private static MouseState _currentMouseState;
        private static GamePadState _currentVirtualState;

        private static GamePadState _lastGamePadState;
        private static KeyboardState _lastKeyboardState;
        private static MouseState _lastMouseState;
        private static GamePadState _lastVirtualState;
        private static bool _handleVirtualStick;

        private static Point _cursor;
        private static bool _cursorIsValid;
        private static bool _cursorIsVisible;
        private static bool _cursorMoved;

#if WINDOWS_PHONE
        private static VirtualStick _phoneStick;
        private static VirtualButton _phoneA;
        private static VirtualButton _phoneB;
#endif

        public static Action<Keys> OnNewKey;
        private static readonly LinkedList<Keys> _lastKeys = new LinkedList<Keys>();


        /// <summary>
        ///     Constructs a new input state.
        /// </summary>
        public static void Initialize()
        {
            _currentKeyboardState = new KeyboardState();
            _currentGamePadState = new GamePadState();
            _currentMouseState = new MouseState();
            _currentVirtualState = new GamePadState();

            _lastKeyboardState = new KeyboardState();
            _lastGamePadState = new GamePadState();
            _lastMouseState = new MouseState();
            _lastVirtualState = new GamePadState();

            _cursorIsVisible = false;
            _cursorMoved = false;
#if WINDOWS_PHONE
            _cursorIsValid = false;
#else
            _cursorIsValid = true;
#endif
            _cursor = Point.Zero;

            _handleVirtualStick = false;
        }

        public static GamePadState GamePadState
        {
            get { return _currentGamePadState; }
        }

        public static KeyboardState KeyboardState
        {
            get { return _currentKeyboardState; }
        }

        public static MouseState MouseState
        {
            get { return _currentMouseState; }
        }

        public static GamePadState VirtualState
        {
            get { return _currentVirtualState; }
        }

        public static GamePadState PreviousGamePadState
        {
            get { return _lastGamePadState; }
        }

        public static KeyboardState PreviousKeyboardState
        {
            get { return _lastKeyboardState; }
        }

        public static MouseState PreviousMouseState
        {
            get { return _lastMouseState; }
        }

        public static GamePadState PreviousVirtualState
        {
            get { return _lastVirtualState; }
        }

        public static bool ShowCursor
        {
            get { return _cursorIsVisible && _cursorIsValid; }
            set { _cursorIsVisible = value; }
        }

        public static bool EnableVirtualStick
        {
            get { return _handleVirtualStick; }
            set { _handleVirtualStick = value; }
        }

        public static Point Cursor
        {
            get { return _cursor; }
        }

        public static bool IsCursorMoved
        {
            get { return _cursorMoved; }
        }

        public static bool IsCursorValid
        {
            get { return _cursorIsValid; }
        }

        public static void LoadContent()
        {
        }

        /// <summary>
        ///     Reads the latest state of the keyboard and gamepad and mouse/touchpad.
        /// </summary>
        public static void Update(GameTime gameTime)
        {
            _lastKeyboardState = _currentKeyboardState;
            _lastGamePadState = _currentGamePadState;
            _lastMouseState = _currentMouseState;
            if (_handleVirtualStick)
                _lastVirtualState = _currentVirtualState;

            _currentKeyboardState = Keyboard.GetState();
            //_currentGamePadState = GamePad.GetState(PlayerIndex.One);
            _currentMouseState = Mouse.GetState();

            var keys = _currentKeyboardState.GetPressedKeys();
            if (OnNewKey != null && keys.Length != _lastKeys.Count)
            {
                foreach (var k in keys.Where(k => _lastKeys.Find(k) == null))
                {
                    OnNewKey(k);
                }

                _lastKeys.Clear();
                foreach (var t in keys)
                {
                    _lastKeys.AddLast(t);
                }
            }

            if (_handleVirtualStick)
            {
#if XBOX
                _currentVirtualState= GamePad.GetState(PlayerIndex.One);
#elif WINDOWS
                //_currentVirtualState = GamePad.GetState(PlayerIndex.One).IsConnected ? GamePad.GetState(PlayerIndex.One) : HandleVirtualStickWin();
#endif
            }


            // Update cursor
            var oldCursor = _cursor;
            if (_currentGamePadState.IsConnected && _currentGamePadState.ThumbSticks.Left != Vector2.Zero)
            {
                var temp = _currentGamePadState.ThumbSticks.Left;
                _cursor.X += (int) (temp.X*new Vector2(300f, -300f).X*(float) gameTime.ElapsedGameTime.TotalSeconds);
                _cursor.Y += (int) (temp.Y*new Vector2(300f, -300f).Y*(float) gameTime.ElapsedGameTime.TotalSeconds);
                Mouse.SetPosition(_cursor.X, _cursor.Y);
            }
            else
            {
                _cursor.X = _currentMouseState.X;
                _cursor.Y = _currentMouseState.Y;
            }

            if (_cursorIsValid && oldCursor != _cursor)
                _cursorMoved = true;
            else
                _cursorMoved = false;
        }

        public static void Draw()
        {
        }

        private static GamePadState HandleVirtualStickWin()
        {
            var _leftStick = Vector2.Zero;
            var _buttons = new List<Buttons>();

            if (_currentKeyboardState.IsKeyDown(Keys.A))
                _leftStick.X -= 1f;
            if (_currentKeyboardState.IsKeyDown(Keys.S))
                _leftStick.Y -= 1f;
            if (_currentKeyboardState.IsKeyDown(Keys.D))
                _leftStick.X += 1f;
            if (_currentKeyboardState.IsKeyDown(Keys.W))
                _leftStick.Y += 1f;
            if (_currentKeyboardState.IsKeyDown(Keys.Space))
                _buttons.Add(Buttons.A);
            if (_currentKeyboardState.IsKeyDown(Keys.LeftControl))
                _buttons.Add(Buttons.B);
            if (_leftStick != Vector2.Zero)
                _leftStick.Normalize();

            return new GamePadState(_leftStick, Vector2.Zero, 0f, 0f, _buttons.ToArray());
        }

        private static GamePadState HandleVirtualStickWP7()
        {
            var _buttons = new List<Buttons>();
            var _stick = Vector2.Zero;
#if WINDOWS_PHONE
            _phoneA.Pressed = false;
            _phoneB.Pressed = false;
            TouchCollection touchLocations = TouchPanel.GetState();
            foreach (TouchLocation touchLocation in touchLocations)
            {
                _phoneA.Update(touchLocation);
                _phoneB.Update(touchLocation);
                _phoneStick.Update(touchLocation);
            }
            if (_phoneA.Pressed)
            {
                _buttons.Add(Buttons.A);
            }
            if (_phoneB.Pressed)
            {
                _buttons.Add(Buttons.B);
            }
            _stick = _phoneStick.StickPosition;
#endif
            return new GamePadState(_stick, Vector2.Zero, 0f, 0f, _buttons.ToArray());
        }

        /// <summary>
        ///     Helper for checking if a key was newly pressed during this update.
        /// </summary>
        public static bool IsNewKeyPress(Keys key)
        {
            return (_currentKeyboardState.IsKeyDown(key) && _lastKeyboardState.IsKeyUp(key));
        }

        public static bool IsNewKeyRelease(Keys key)
        {
            return (_lastKeyboardState.IsKeyDown(key) && _currentKeyboardState.IsKeyUp(key));
        }

        /// <summary>
        ///     Helper for checking if a button was newly pressed during this update.
        /// </summary>
        public static bool IsNewButtonPress(Buttons button)
        {
            return (_currentGamePadState.IsButtonDown(button) && _lastGamePadState.IsButtonUp(button));
        }

        public static bool IsNewButtonRelease(Buttons button)
        {
            return (_lastGamePadState.IsButtonDown(button) && _currentGamePadState.IsButtonUp(button));
        }

        /// <summary>
        ///     Helper for checking if a mouse button was newly pressed during this update.
        /// </summary>
        public static bool IsNewMouseButtonPress(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (_currentMouseState.LeftButton == ButtonState.Pressed && _lastMouseState.LeftButton == ButtonState.Released);
                case MouseButtons.RightButton:
                    return (_currentMouseState.RightButton == ButtonState.Pressed && _lastMouseState.RightButton == ButtonState.Released);
                case MouseButtons.MiddleButton:
                    return (_currentMouseState.MiddleButton == ButtonState.Pressed && _lastMouseState.MiddleButton == ButtonState.Released);
                case MouseButtons.ExtraButton1:
                    return (_currentMouseState.XButton1 == ButtonState.Pressed && _lastMouseState.XButton1 == ButtonState.Released);
                case MouseButtons.ExtraButton2:
                    return (_currentMouseState.XButton2 == ButtonState.Pressed && _lastMouseState.XButton2 == ButtonState.Released);
                default:
                    return false;
            }
        }


        /// <summary>
        ///     Checks if the requested mouse button is released.
        /// </summary>
        /// <param name="button">The button.</param>
        public static bool IsNewMouseButtonRelease(MouseButtons button)
        {
            switch (button)
            {
                case MouseButtons.LeftButton:
                    return (_lastMouseState.LeftButton == ButtonState.Pressed && _currentMouseState.LeftButton == ButtonState.Released);
                case MouseButtons.RightButton:
                    return (_lastMouseState.RightButton == ButtonState.Pressed && _currentMouseState.RightButton == ButtonState.Released);
                case MouseButtons.MiddleButton:
                    return (_lastMouseState.MiddleButton == ButtonState.Pressed && _currentMouseState.MiddleButton == ButtonState.Released);
                case MouseButtons.ExtraButton1:
                    return (_lastMouseState.XButton1 == ButtonState.Pressed && _currentMouseState.XButton1 == ButtonState.Released);
                case MouseButtons.ExtraButton2:
                    return (_lastMouseState.XButton2 == ButtonState.Pressed && _currentMouseState.XButton2 == ButtonState.Released);
                default:
                    return false;
            }
        }

        /// <summary>
        ///     Checks for a "menu select" input action.
        /// </summary>
        public static bool IsMenuSelect()
        {
            return IsNewKeyPress(Keys.Space) || IsNewKeyPress(Keys.Enter) || IsNewButtonPress(Buttons.A) || IsNewButtonPress(Buttons.Start) ||
                   IsNewMouseButtonPress(MouseButtons.LeftButton);
        }

        public static bool IsMenuPressed()
        {
            return _currentKeyboardState.IsKeyDown(Keys.Space) || _currentKeyboardState.IsKeyDown(Keys.Enter) || _currentGamePadState.IsButtonDown(Buttons.A) ||
                   _currentGamePadState.IsButtonDown(Buttons.Start) || _currentMouseState.LeftButton == ButtonState.Pressed;
        }

        public static bool IsMenuReleased()
        {
            return IsNewKeyRelease(Keys.Space) || IsNewKeyRelease(Keys.Enter) || IsNewButtonRelease(Buttons.A) || IsNewButtonRelease(Buttons.Start) ||
                   IsNewMouseButtonRelease(MouseButtons.LeftButton);
        }

        /// <summary>
        ///     Checks for a "menu cancel" input action.
        /// </summary>
        public static bool IsMenuCancel()
        {
            return IsNewKeyPress(Keys.Escape) || IsNewButtonPress(Buttons.Back);
        }
    }
}